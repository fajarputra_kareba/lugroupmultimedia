<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posisi extends Model
{
    protected $table = 'posisi';
    protected $primaryKey = 'pos_id';
    public $timestamps = false;
    protected $fillable = [
        'pos_id','user_id','pos_department', 'pos_jabatan',
        'pos_tipe', 'pos_mulaikerja', 'pos_cabang', 'pos_date_update'
    ];

}
