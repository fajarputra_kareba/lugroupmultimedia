<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jenisjabatan extends Model
{
  protected $table = 'jenis_jabatan';
  protected $primaryKey = 'jab_id';
  public $timestamps = false;
  protected $fillable = [
      'jab_id','jab_nama','jab_kode','jab_atasan','jab_department','jab_created'
  ];
}
