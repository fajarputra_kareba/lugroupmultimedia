<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lembur extends Model
{
  protected $table = 'lembur';
  protected $primaryKey = 'lem_id';
  public $timestamps = false;
  protected $fillable = [
      'lem_id','lem_user_id','lem_tujuan','lem_tgl_lembur','lem_time_start', 'lem_time_end', 'lem_keterangan', 'lem_created'
  ];
}
