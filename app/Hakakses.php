<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hakakses extends Model
{
  protected $table = 'hak_akses';
  protected $primaryKey = 'hak_id';
  public $timestamps = false;
  protected $fillable = [
      'hak_id','hak_ug_id','hak_mod_id','hak_akses','hak_add','hak_edit','hak_delete'
  ];
}
