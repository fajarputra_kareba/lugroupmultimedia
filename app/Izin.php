<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Izin extends Model
{
  protected $table = 'izin';
  protected $primaryKey = 'izin_id';
  public $timestamps = false;
  protected $fillable = [
      'izin_id','izin_user_id','izin_jenis_id','izin_date_start', 'izin_date_end', 'izin_keterangan', 'izin_personalia_ket', 'izin_gm_ket', 'izin_status', 'izin_created'
  ];

}
