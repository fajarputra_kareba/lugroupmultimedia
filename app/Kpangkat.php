<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kpangkat extends Model
{
  protected $table = 'kenaikan_pangkat';
  protected $primaryKey = 'kp_id';
  public $timestamps = false;
  protected $fillable = [
      'kp_id','kp_nama','kp_created'
  ];
}
