<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Berita;
use App\Menu;
use App\Submenu;
use App\Modul;
use DB;

class DefaultController extends Controller
{
  public function index()
  {
    $users = User::join('posisi','users.id','=','posisi.user_id')->paginate(10);
    $berita = Berita::Orderby('be_id','DESC')->take(1)->get();
    $menu = Menu::OrderBy('m_urutan')->get();
    $submenu = Submenu::join('menu', 'submenu.sub_m_id', '=', 'menu.m_id');
    return view('default.default', compact('users','berita','menu','submenu'));
  }
}
