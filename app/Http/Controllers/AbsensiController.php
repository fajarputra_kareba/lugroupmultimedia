<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Usergroup;
use App\Menu;
use App\Modul;
use App\Hakakses;
use App\User;
use App\Department;
use App\Jabatan;
use App\Jamkerja;
use App\Harikerja;
use App\Jenisizin;
use App\Lembur;
use DB;

class AbsensiController extends Controller
{
  public function index()
  {
    $menu = Menu::All();
    $users = User::All();
    return view('absensi.dataabsensi', compact('menu','users'));
  }

  function Parse_Data($data,$p1,$p2){
    $data=" ".$data;
    $hasil="";
    $awal=strpos($data,$p1);
    if($awal!=""){
    $akhir=strpos(strstr($data,$p1),$p2);
        if($akhir!=""){
        $hasil=substr($data,$awal+strlen($p1),$akhir-strlen($p1));
        }
    }
    return $hasil;
}

  public function get_setting(){
    	$data = DB::table('pengaturan')->get();
    	return $data;
    }

    public function if_exist_check($PIN, $DateTime){
        $data = DB::table('data_absen')->where('pin', $PIN)
                ->where('date_time', $DateTime);
        return $data;
    }

	public function get_data_absen(){
		error_reporting(0);
        $IP = $this->get_setting()->ip;
        $Key = $this->get_setting()->password;
        if($IP!=""){
        $Connect = fsockopen($IP, "80", $errno, $errstr, 1);
            if($Connect){
                $soap_request="<GetAttLog><ArgComKey xsi:type=\"xsd:integer\">".$Key."</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">All</PIN></Arg></GetAttLog>";
                $newLine="\r\n";
                fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
                fputs($Connect, "Content-Type: text/xml".$newLine);
                fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
                fputs($Connect, $soap_request.$newLine);
                $buffer="";
                while($Response=fgets($Connect, 1024)){
                    $buffer=$buffer.$Response;
                }
                $buffer = $this->Parse_Data($buffer,"<GetAttLogResponse>","</GetAttLogResponse>");
                $buffer = explode("\r\n",$buffer);
                for($a=0;$a<count($buffer);$a++){
                    $data = Parse_Data($buffer[$a],"<Row>","</Row>");
                    $PIN = Parse_Data($data,"<PIN>","</PIN>");
                    $DateTime = Parse_Data($data,"<DateTime>","</DateTime>");
                    $Verified = Parse_Data($data,"<Verified>","</Verified>");
                    $Status = Parse_Data($data,"<Status>","</Status>");
                    $ins = array(
                            "pin"       =>  $PIN,
                            "date_time" =>  $DateTime,
                            "ver"		=>  $Verified,
                            "status"    =>  $Status
                            );
                    if (!$this->if_exist_check($PIN, $DateTime) && $PIN && $DateTime) {
                    	$this->db->insert('data_absen', $ins);
                    }
                }
                if($buffer){
                	return '<div class="alert alert-success alert-dismissable">
        				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        				<h4><i class="icon fa fa-check"></i> Success !</h4>
        				Anda terhubung dengan mesin.
        			</div>';
                } else {
                	return '<div class="alert alert-danger alert-dismissable">
        				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        				<h4><i class="icon fa fa-ban"></i> Alert!</h4>
        				Anda tidak terhubung dengan mesin !
        			</div>';
                }
            }
        }
    }

  public function rekap()
  {
    error_reporting(0);
        $IP = "192.168.98.11";
        $Key = "0";
        if($IP!=""){
        $Connect = fsockopen($IP, "80", $errno, $errstr, 1);
            if($Connect){
                $soap_request="<GetAttLog><ArgComKey xsi:type=\"xsd:integer\">".$Key."</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">All</PIN></Arg></GetAttLog>";
                $newLine="\r\n";
                fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
                fputs($Connect, "Content-Type: text/xml".$newLine);
                fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
                fputs($Connect, $soap_request.$newLine);
                $buffer="";
                while($Response=fgets($Connect, 1024)){
                    $buffer=$buffer.$Response;
                }
                $buffer = $this->Parse_Data($buffer,"<GetAttLogResponse>","</GetAttLogResponse>");
                $buffer = explode("\r\n",$buffer);
                for($a=0;$a<count($buffer);$a++){
                    $data = $this->Parse_Data($buffer[$a],"<Row>","</Row>");
                    $PIN = $this->Parse_Data($data,"<PIN>","</PIN>");
                    $DateTime = $this->Parse_Data($data,"<DateTime>","</DateTime>");
                    $Verified = $this->Parse_Data($data,"<Verified>","</Verified>");
                    $Status = $this->Parse_Data($data,"<Status>","</Status>");
                    $ins = array(
                            "pin"       =>  $PIN,
                            "date_time" =>  $DateTime,
                            "ver"		=>  $Verified,
                            "status"    =>  $Status
                            );
                    if (!$this->if_exist_check($PIN, $DateTime) && $PIN && $DateTime) {
                      DB::table('data_absen')
                         ->insert([
                               'pin' => $PIN,
                               'date_time' => $DateTime,
                               'ver' => $Verified,
                               'status' => $Status
                       ]);
                    }
                }

            }
        }
      $menu = Menu::All();
      $users = DB::table('users')->paginate(10);
      $pegawai = User::All();
      $department = Department::All();
      $jabatan = Jabatan::All();

      return view('absensi.rekapabsensi', compact('users', 'department', 'jabatan', 'menu', 'pegawai'));
  }

  public function filterrekap()
  {
    error_reporting(0);
        $IP = "192.168.98.11";
        $Key = "0";
        if($IP!=""){
        $Connect = fsockopen($IP, "80", $errno, $errstr, 1);
            if($Connect){
                $soap_request="<GetAttLog><ArgComKey xsi:type=\"xsd:integer\">".$Key."</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">All</PIN></Arg></GetAttLog>";
                $newLine="\r\n";
                fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
                fputs($Connect, "Content-Type: text/xml".$newLine);
                fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
                fputs($Connect, $soap_request.$newLine);
                $buffer="";
                while($Response=fgets($Connect, 1024)){
                    $buffer=$buffer.$Response;
                }
                $buffer = $this->Parse_Data($buffer,"<GetAttLogResponse>","</GetAttLogResponse>");
                $buffer = explode("\r\n",$buffer);
                for($a=0;$a<count($buffer);$a++){
                    $data = $this->Parse_Data($buffer[$a],"<Row>","</Row>");
                    $PIN = $this->Parse_Data($data,"<PIN>","</PIN>");
                    $DateTime = $this->Parse_Data($data,"<DateTime>","</DateTime>");
                    $Verified = $this->Parse_Data($data,"<Verified>","</Verified>");
                    $Status = $this->Parse_Data($data,"<Status>","</Status>");
                    $ins = array(
                            "pin"       =>  $PIN,
                            "date_time" =>  $DateTime,
                            "ver"		=>  $Verified,
                            "status"    =>  $Status
                            );
                    if (!$this->if_exist_check($PIN, $DateTime) && $PIN && $DateTime) {
                      DB::table('data_absen')
                         ->insert([
                               'pin' => $PIN,
                               'date_time' => $DateTime,
                               'ver' => $Verified,
                               'status' => $Status
                       ]);
                    }
                }

            }
        }
      $mount = request('bulan');
      $tahun = request('tahun');
      $pegawai = User::All();
      $nama = request('name');
      if ($nama != NULL) {
        $nama = request('name');
        $users = DB::table('users')
            ->where('id', $nama)
            ->paginate(10);
      }else {
        $users = DB::table('users')
            ->paginate(10);
      }
      $menu = Menu::All();

      $department = Department::All();
      $jabatan = Jabatan::All();

      return view('absensi.filterrekapabsen', compact('users', 'department', 'jabatan', 'menu', 'mount', 'tahun', 'pegawai'));
  }

  public function inoutlog()
  {
    error_reporting(0);
        $IP = "192.168.98.11";
        $Key = "0";
        if($IP!=""){
        $Connect = fsockopen($IP, "80", $errno, $errstr, 1);
            if($Connect){
                $soap_request="<GetAttLog><ArgComKey xsi:type=\"xsd:integer\">".$Key."</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">All</PIN></Arg></GetAttLog>";
                $newLine="\r\n";
                fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
                fputs($Connect, "Content-Type: text/xml".$newLine);
                fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
                fputs($Connect, $soap_request.$newLine);
                $buffer="";
                while($Response=fgets($Connect, 1024)){
                    $buffer=$buffer.$Response;
                }
                $buffer = $this->Parse_Data($buffer,"<GetAttLogResponse>","</GetAttLogResponse>");
                $buffer = explode("\r\n",$buffer);
                for($a=0;$a<count($buffer);$a++){
                    $data = $this->Parse_Data($buffer[$a],"<Row>","</Row>");
                    $PIN = $this->Parse_Data($data,"<PIN>","</PIN>");
                    $DateTime = $this->Parse_Data($data,"<DateTime>","</DateTime>");
                    $Verified = $this->Parse_Data($data,"<Verified>","</Verified>");
                    $Status = $this->Parse_Data($data,"<Status>","</Status>");
                    $ins = array(
                            "pin"       =>  $PIN,
                            "date_time" =>  $DateTime,
                            "ver"		=>  $Verified,
                            "status"    =>  $Status
                            );
                    if (!$this->if_exist_check($PIN, $DateTime) && $PIN && $DateTime) {
                      DB::table('data_absen')
                         ->insert([
                               'pin' => $PIN,
                               'date_time' => $DateTime,
                               'ver' => $Verified,
                               'status' => $Status
                       ]);
                    }
                }

            }
        }
      $menu = Menu::All();
      $users = DB::table('users')->paginate(10);
      $department = Department::All();
      $jabatan = Jabatan::All();

      return view('absensi.inoutlog', compact('users', 'department', 'jabatan','menu'));
  }

  public function insert_uid(Request $req)
  {
    $date = date('Y-m-d H:i:s');
    $input = $req->all();
    $jumlah = count($req['user']);
    for ($i=0; $i < $jumlah; $i++) {

       DB::table('data_absen')
          ->insert([
                'data_user_id' => $req['user'][$i],
                'data_uid_mesin' => $req['uid1'][$i],
                'data_uid_mesin2' => $req['uid2'][$i],
                'datetime' => $date
        ]);

    }

    //dd($input);

    return redirect()->back();

  }

  public function view_uid()
  {
    $menu = Menu::All();
    $user = User::All();
    $uid = DB::table('users')->join('data_absen','users.id','=','data_absen.data_user_id')->get();
    return view('absensi.viewuid', compact('menu','uid','user'));
  }

  public function update_uid(Request $req)
  {
    $date = date("Y-m-d h:i:s");
    $input = $req->all();
    $jumlah = count($req['user']);
    for ($i=0; $i < $jumlah; $i++) {

      DB::table('data_absen')
           ->where('data_id', $req['id'][$i])
           ->update([
                 'data_user_id' => $req['user'][$i],
                 'data_uid_mesin' => $req['uid1'][$i],
                 'data_uid_mesin2' => $req['uid2'][$i],
                 'datetime' => $date

         ]);
      }
    //dd($input);

    return redirect()->back();
  }

  public function jenisjamkerja()
  {
      $menu = Menu::All();
      $jeniskerja = Jamkerja::paginate(10);
      return view('absensi.jamkerja', compact('jeniskerja','menu'));
  }

  public function create_jamkerja()
  {
      $menu = Menu::All();
      return view('absensi.tambahjamkerja', compact('menu'));
  }

  public function insert_jamkerja(Request $req)
  {
    $hari = implode(',', request('hari'));
    $date = date("Y-m-d h:i:s");
    Jamkerja::create([
        'jam_jenis_kerja' => request('jenis'),
        'jam_auto' => request('auto'),
        'jam_hari_libur' => request('libur'),
        'jam_hari_kerja' => $hari,
        'jam_datetime' => $date,

    ]);

    $data = Jamkerja::Where('jam_jenis_kerja', request('jenis'))->first();
    $input = $data->jam_id;
    $exhari = explode(',', $data->jam_hari_kerja);
    $jumhari = count($req['day']);
    $harijam = $req->all();
    $none = "00:00";

    for ($i=0; $i < $jumhari ; $i++) {
      $masuk = $req['masuk'][$i];
      $akhirmasuk = $req['akhirmasuk'][$i];
      $pulang = $req['pulang'][$i];
      $akhirpulang = $req['akhirpulang'][$i];
      $mulailembur = $req['mulailembur'][$i];
      $akhirlembur = $req['akhirlembur'][$i];
      if ($req['masuk'][$i] == "") {
        $masuk = $none;
      }
      if ($req['akhirmasuk'][$i] == "") {
        $akhirmasuk = $none;
      }
      if ($req['pulang'][$i] == "") {
        $pulang = $none;
      }
      if ($req['akhirpulang'][$i] == "") {
        $akhirpulang = $none;
      }
      if ($req['mulailembur'][$i] == "") {
        $mulailembur = $none;
      }
      if ($req['akhirlembur'][$i] == "") {
        $akhirlembur = $none;
      }


      Harikerja::create([
          'hari_jam_id' => $input,
          'hari_nama' => $req['day'][$i],
          'hari_jam_masuk' => $masuk,
          'hari_jam_akhir_masuk' => $akhirmasuk,
          'hari_jam_pulang' => $pulang,
          'hari_jam_akhir_pulang' => $akhirpulang,
          'hari_jam_lembur' => $mulailembur,
          'hari_selesai_lembur' => $akhirlembur,
          'hari_datetime' => $date,

      ]);
    }



    return redirect()->back();
  }

  public function destroy($id)
  {
      $kerja = Jamkerja::find($id);
      $kerja->delete();
      $hari = DB::table('hari_kerja')->where('hari_jam_id', $id);
      $hari->delete();
      return redirect()->route('jamkerja');
  }

  public function lembur()
  {
      $pegawai = User::All();
      $menu = Menu::All();
      $users = DB::table('users')->paginate(10);
      $department = Department::All();
      $jabatan = Jabatan::All();
      $lembur = Lembur::join('users','lembur.lem_user_id','=','users.id')
                      ->OrderBy('lembur.lem_id', 'ASC')
                      ->paginate(10);

      return view('absensi.formlembur', compact('users', 'department', 'jabatan', 'menu', 'pegawai', 'lembur'));
  }

  public function editlembur($id)
  {
      $menu = Menu::All();
      $lembur = Lembur::join('users','lembur.lem_user_id','=','users.id')
                      ->where('lembur.lem_id', $id)
                      ->get();
      $jenisizin = Jenisizin::All();
      $users = User::All();
      $department = Department::All();
      $pegawai = User::All();
      // dd($izin);
      return view('absensi.editlembur', compact('lembur','menu','jenisizin','users','department','pegawai'));
  }

  public function createlembur()
  {
      $users = User::All();
      $department = Department::All();
      $pegawai = User::All();
      $menu = Menu::All();
      return view('absensi.createlembur', compact('users', 'department', 'menu', 'pegawai'));
  }

  public function insertlembur()
  {
    $date = date('Y-m-d H:i:s');

       DB::table('lembur')
          ->insert([
                'lem_user_id' => request('user'),
                'lem_tujuan' => request('tujuan'),
                'lem_tgl_lembur' => request('tgl_lembur'),
                'lem_time_start' => request('start'),
                'lem_time_end' => request('end'),
                'lem_keterangan' => request('keterangan'),
                'lem_valid' => "Pending",
                'lem_created' => $date
        ]);


    return redirect()->back();

  }

  public function updatelembur($id)
  {
    $date = date('Y-m-d H:i:s');

       DB::table('lembur')
          ->where('lem_id', $id)
          ->update([
                'lem_user_id' => request('user'),
                'lem_tujuan' => request('tujuan'),
                'lem_tgl_lembur' => request('tgl_lembur'),
                'lem_time_start' => request('start'),
                'lem_time_end' => request('end'),
                'lem_keterangan' => request('keterangan'),
                'lem_valid' => "Pending",
                'lem_created' => $date
        ]);


    return redirect()->back();

  }


}
