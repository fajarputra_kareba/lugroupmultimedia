<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Usergroup;
use App\Menu;
use App\Modul;
use App\Hakakses;
use App\User;
use App\Department;
use App\Jabatan;
use App\Jamkerja;
use App\Harikerja;
use DB;

class DepartmentController extends Controller
{
  public function departemen()
  {

      $pegawai = User::All();
      $menu = Menu::All();
      $users = DB::table('users')->paginate(10);
      $department = Department::paginate(10);
      $jabatan = Jabatan::All();

      return view('departemen.departemen', compact('users', 'department', 'jabatan', 'menu', 'pegawai'));
  }

  public function createdepartemen()
  {

      $menu = Menu::All();
      return view('departemen.createdepartemen', compact('menu'));
  }

  public function insertdepartemen()
  {
    $date = date('Y-m-d H:i:s');

       DB::table('department')
          ->insert([
                'dept_nama' => request('nama'),
                'dept_kode' => request('kode'),
                'dept_update' => $date
        ]);


    return redirect()->back();

  }

  public function destroydepartemen($id)
  {
      $department = Department::find($id);
      $department->delete();
      return redirect()->route('departemen');
  }
}
