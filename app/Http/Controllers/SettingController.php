<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Usergroup;
use App\Menu;
use App\Modul;
use App\Hakakses;
use DB;


class SettingController extends Controller
{
  public function index()
  {
    $menu = Menu::All();
    $group = Usergroup::All();
    return view('setting.usergroup', compact('group','menu'));
  }

  public function create_usergroup()
  {
      $menu = Menu::All();
      return view('setting.usergroup_insert', compact('menu'));
  }

  public function insert_usergroup()
  {
      $date = date('Y-m-d H:i:s');
      Usergroup::create([
          'ug_usergroup' => request('ug'),
          'ug_label' => request('uglabel'),
          'ug_date' => $date

      ]);

      return redirect()->route('setting.usergroup');
  }

  public function destroyug($id)
  {
      $ug = Usergroup::find($id);
      $ug->delete();
      return redirect()->route('ug');
  }

  public function viewhakakses()
  {
    $menu = Menu::All();
    $modul = Modul::All();
    $group = Usergroup::All();
    $hakakses = Hakakses::Distinct()->get(['hak_ug_id']);
    return view('setting.viewhakakses', compact('modul','menu','group','hakakses'));
  }

  public function hakakses()
  {
    $menu = Menu::All();
    $modul = Modul::All();
    $group = Usergroup::All();
    $selectedGroup = Usergroup::first()->id;
    return view('setting.hakakses', compact('modul','menu','group','selectedGroup'));
  }

  public function hakinsert(Request $request)
  {
      $data = $request->except('_token','_method');
      $date = date("Y-m-d h:i:s");
      $jumlah = Modul::count();

      for($i=0; $i < $jumlah; $i++){

  			$ha = new Hakakses;
  			$ha->hak_ug_id = $data['ugid'];
        $ha->hak_mod_id = $data['modul'][$i];
        $ha->hak_akses = $data['akses'][$i];
        $ha->hak_add = $data['add'][$i];
        $ha->hak_edit = $data['edit'][$i];
        $ha->hak_delete = $data['delete'][$i];
        $ha->hak_date = date("Y-m-d h:i:s");
  			$ha->save();
  		}

      return redirect()->route('setting.hakakses');
  }

  public function edit_hakakses($id)
  {
    $menu = Menu::All();
    $modul = Modul::All();
    $group = Usergroup::All();
    $selectedGroup = Usergroup::first()->id;
    $hakakses = DB::table("hak_akses")->where('hak_id', '=', $id)->get();

    return view('setting.updatehakakses', compact('modul','menu','group','selectedGroup','hakakses'));
  }

  public function update_hakakses(Request $req)
  {
    $input = $req->all();
    $date = date("Y-m-d h:i:s");
    $jumlah = Modul::count();
    for ($i=0; $i < $jumlah; $i++) {

       DB::table('hak_akses')
            ->where('hak_ug_id',$input['ugid'])
            ->where('hak_mod_id',$input['modul'][$i])
            ->update([
                'hak_akses' => $input['akses'][$i],
                'hak_add' => $input['add'][$i],
                'hak_edit' => $input['edit'][$i],
                'hak_delete' => $input['delete'][$i],

        ]);

    }

    //dd($input);

    return redirect()->back();

  }

  public function modul()
  {
    $menu = Menu::All();
    $modul = Modul::All();
    return view('setting.modul', compact('modul','menu'));
  }

  public function create_modul()
  {
      $menu = Menu::All();
      return view('setting.tambahmodul', compact('menu'));
  }


  public function insert_modul(Request $req)
  {

    $date = date("Y-m-d h:i:s");
    Modul::create([
        'mod_label' => request('modlabel'),
        'mod_alias' => request('modalias'),
        'mod_date' => $date,

    ]);

    //dd($input);

    return redirect()->back();

  }

  public function edit_modul($id)
  {
      $menu = Menu::All();
      $modul = DB::table("modul")->where('mod_id', '=', $id)->get();
      $modlink = DB::table("modlink")->where('mol_mod_id', '=', $id)->get();
      return view('setting.updatemodul', compact('menu','modul','modlink'));
  }

  public function update_modul(Request $req)
  {
    $date = date("Y-m-d h:i:s");
    $modul = DB::table("modul")->where('mod_id', '=', request('idmod'));

    $modul->update([
        'mod_label' => request('modlabel'),
        'mod_alias' => request('modalias'),
        'mod_date' => $date,

    ]);

    $input = $req->all();
    $jumlah = count($req['label']);
    for ($i=0; $i < $jumlah; $i++) {

      if ($req['mol_id'][$i] == NULL) {
        DB::table('modlink')
           ->insert([
                 'mol_mod_id' => request('idmod'),
                 'mol_nama' => $req['label'][$i],
                 'mol_link' => $req['link'][$i],
                 'mol_date' => $date

         ]);
      }else {
        DB::table('modlink')
           ->where('mol_id', $req['mol_id'][$i])
           ->update([
                 'mol_mod_id' => request('idmod'),
                 'mol_nama' => $req['label'][$i],
                 'mol_link' => $req['link'][$i],
                 'mol_date' => $date

         ]);
      }


    }

    //dd($input);

    return redirect()->back();
  }

  public function destroy_modul($id)
  {
      $modlink = DB::table("modlink")->where('mol_id',$id);
      $modlink->delete();
      return redirect()->back();
  }

}
