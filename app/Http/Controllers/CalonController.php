<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Calon;
use \App\User;
use \App\Menu;
use DB;

class CalonController extends Controller
{
  public function index()
  {
      $menu = Menu::All();
      $calon = Calon::All();
      $calon = DB::table('calon_pegawai')->where('status','pending')->paginate(5);
      return view('calon.calon', compact('calon','menu'));
  }

  public function penilaian($id)
  {
      $menu = Menu::All();
      $penilaian = DB::table("calon_pegawai")->where('cp_id', '=', $id)->get();
      return view('calon.penilaian', compact('penilaian','menu'));
  }

  public function seleksi()
  {
      $menu = Menu::All();
      $calon = Calon::All();
      $calon = DB::table('calon_pegawai')->where('status','Setujui')->paginate(5);
      return view('calon.seleksi', compact('calon','menu'));
  }

  public function hasil()
  {
      $menu = Menu::All();
      $calon = Calon::All();
      $calon = DB::table('calon_pegawai')
                ->where('cp_hasil','Accept')
                ->where('cp_transfer','')
                ->paginate(5);
      return view('calon.hasil', compact('calon','menu'));
  }

  public function create()
  {
      $menu = Menu::All();
      $calon = Calon::All();
      return view('calon.calonpegawai', compact('calon','menu'));
  }

  public function insert(Request $request)
  {

      $file       = $request->file('gambar');
      $fileName   = $file->getClientOriginalName();
      $request->file('gambar')->move("image/", $fileName);

      $filecv       = $request->file('filecv');
      $fileNameCv   = $filecv->getClientOriginalName();
      $request->file('filecv')->move("image/", $fileNameCv);
      Calon::create([
          'cp_fullname' => request('name'),
          'cp_telp' => request('telepon'),
          'cp_alamat' => request('alamat'),
          'cp_gender' => request('gender'),
          'cp_filecv' => $fileNameCv,
          'cp_foto' => $fileName,
          'cp_skill' => request('skill'),
          'cp_pengalaman' => request('pengalaman'),
          'status' => 'pending',
          'cp_penilaian' => 'kosong',
          'cp_hasil' => 'kosong',
          'cp_tgl_transfer' => NULL,
          'cp_transfer' => 'kosong'

      ]);

      return redirect()->route('calon.index');
  }

  public function employe(Request $request, $id)
  {

      $tgl = date('Y-m-d');
      $calon = Calon::find($id);
      $calon->update([
        'cp_tgl_transfer' => $tgl,
        'cp_transfer' => 'dipindahkan'
      ]);

        User::create([
            'name' => request('name'),
            'nik' => 'kosong',
            'department' => 'kosong',
            'jabatan' => 'kosong',
            'email' => 'admin@gmail.com',
            'tanggal_kerja' => $tgl,
            'gender' => request('gender'),
            'status' => 'Kontrak',
            'photo' => request('gambar'),
            'password' => bcrypt(request('password'))
        ]);

      return redirect()->route('pegawai.kontrak');
  }

  public function approve($id)
  {

      $calon = Calon::find($id);
      $calon->update([
        'status' => 'Setujui'
      ]);

      return redirect()->route('calon.index');
  }

  public function nilai($id)
  {

      $calon = Calon::find($id);
      $poinnilai = implode(',', request('nilai'));
      $calon->update([
        'cp_penilaian' => $poinnilai,
        'cp_hasil' => 'Accept'
      ]);

      return redirect()->route('calon.index');
  }

  public function deleteAll(Request $request)
  {
      $ids = $request->ids;
      DB::table("calon_pegawai")->whereIn('cp_id',explode(",",$ids))->delete();
      return response()->json(['success'=>"Products Deleted successfully."]);
  }
}
