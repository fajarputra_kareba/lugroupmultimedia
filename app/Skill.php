<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Skill extends Model
{
    protected $table = 'skills';
    protected $primaryKey = 'skill_id';
    protected $fillable = [
        'user_id', 'skill', 'deskripsi',
    ];

    public function user()
    {
       return $this->hasMany(User::class);
    }

}
