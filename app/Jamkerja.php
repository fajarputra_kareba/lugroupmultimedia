<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jamkerja extends Model
{
  protected $table = 'jam_kerja';
  protected $primaryKey = 'jam_id';
  public $timestamps = false;
  protected $fillable = [
      'jam_id','jam_auto','jam_jenis_kerja','jam_hari_libur', 'jam_hari_kerja', 'jam_datetime'
  ];
}
