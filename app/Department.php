<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
  protected $table = 'department';
  protected $primaryKey = 'dept_id';
  public $timestamps = false;
  protected $fillable = [
      'dept_id','dept_nama','dept_kode','dept_update'
  ];
}
