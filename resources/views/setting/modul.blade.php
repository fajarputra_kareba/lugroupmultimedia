@extends('master.master')

@section('body')

  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Modul</li>
    </ul>
  </div>
  <!-- /breadcrumbs line -->


  <!-- Table inside panel body -->
			            <div class="panel panel-default">
			                <div class="panel-heading">
                        <div class="pull-right">
                          <a href="{{ route('setting.create_modul') }}" class="btn btn-xs btn-success" style="margin-bottom:10px; margin-top:10px;"><i class="icon-file-plus"></i>Tambah Data</a>
                        </div>
                        <h4 class="panel-title"> Data Modul
                          <small class="display-block">PT Ilugroup Multimeida Indonesia -</small>
                        </h4>

                      </div>
			                <div class="panel-body">
			                	<div class="table-responsive">
					                <table class="table table-bordered">
					                    <thead>
					                        <tr>
					                            <th>#</th>
					                            <th>Nama Modul</th>
					                            <th>Alias</th>
                                      <th>Modul Link</th>
					                            <th>Aksi</th>
					                        </tr>
					                    </thead>
					                    <tbody>
                                @foreach ($modul as $key => $mod)
                                  <tr>
					                            <td>{{++$key}}</td>
					                            <td>{{$mod->mod_label}}</td>
					                            <td>{{$mod->mod_alias}}</td>
                                      <td>
                                        @php
                                          $id = $mod->mod_id;
                                          $modlink = DB::table('modlink')->where('mol_mod_id',$id)->get();
                                        @endphp
                                        @if ($modlink->count()!=0)
                                          <ul>
                                            @foreach ($modlink as $mol)
                                                <li>{{$mol->mol_nama}} <br /> [{{$mol->mol_link}}]</li>
                                            @endforeach
                                          </ul>
                                        @endif
                                      </td>
					                            <td>
                                        <center>
                                          <a href="{{ route('pegawai.detail', $mod->mod_id) }}" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Detail"><i class="icon-file"></i></a>
                                          <a href="{{ route('setting.edit_modul', $mod->mod_id) }}" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Update"><i class="icon-pencil"></i></a>
                                        </center>

                                      </td>
					                        </tr>
                                @endforeach

					                    </tbody>
					                </table>
				                </div>
			                </div>
				        </div>
				        <!-- /table inside panel body -->
@endsection
