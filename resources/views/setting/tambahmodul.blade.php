@extends('master.master')

@section('body')

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
  <ul class="breadcrumb">
    <li style="color:#fff"><a href="index.html">Home</a></li>
    <li class="active">Kepegawaian</li>
  </ul>
</div>
<!-- /breadcrumbs line -->
<!-- Alert -->
<!-- <div class="alert alert-warning fade in block">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
</div> -->
<!-- /alert -->
<!-- Simple contact form -->
    <form action="{{ route('setting.insert_modul') }}" role="form" method="post">
          {{ csrf_field() }}
          <div class="panel panel-default">
          <div class="panel-heading"><h6 class="panel-title"><i class="icon-pencil3"></i> Form Pengisian Modul</h6></div>
          <div class="panel-body">
				      <div class="panel panel-info">
				         <div class="panel-heading">
			              <h6 class="panel-title"><i class="icon-menu"></i> Modul</h6>
		              </div>
		               <div class="panel-body">
                     <div class="form-group">
                       <div class="row">
                         <div class="col-md-6" style="padding:10px;">
                           <label>Label:</label>
                               <input type="text" name="modlabel" class="form-control">
                         </div>

                         <div class="col-md-6" style="padding:10px;">
                           <label>Alias:</label>
                               <input type="text" name="modalias" class="form-control">
                         </div>


                       </div>
                     </div>

		               </div>
		             </div>
              </div>
              <div class="panel-body">
    				      <div class="panel panel-info">
    				         <div class="panel-heading">
    			              <h6 class="panel-title"><i class="icon-menu"></i> Modul Link</h6>
    		              </div>
    		               <div class="panel-body">
                         <div class="table-responsive">
					                <table class="table table-bordered">
					                    <thead>
					                        <tr>
					                           <th>Label</th>
					                            <th>Link</th>
					                            <th><a href="#" class="addRow"><i class="icon-plus-circle2"></i></a></th>
					                        </tr>
					                    </thead>
					                    <tbody>
					                        <tr>
					                            <td> <input type="text" name="label[]" value="" class="form-control"> </td>
					                            <td><input type="text" name="link[]" value="" class="form-control"></td>
					                            <td><a href="#" class="add remove"><i class="icon-remove2"></i></a></td>
                                  </tr>

                              </tbody>
					                </table>
				                </div>

    		               </div>
    		             </div>
                  </div>
          <div class="panel-body">
          <div class="form-actions text-center">
            <a href="{{route('setting.modul')}}" class="btn btn-danger">Kembali</a>
            <input type="submit" value="Insert" class="btn btn-success">
          </div>

        </div>
      </div>
    </form>
    <!-- /simple contact form -->
    <script type="text/javascript">
      $('.addRow').on('click',function(){
        addRow();
      });
      function addRow()
      {
        var tr = '<tr>'+
            '<td> <input type="text" name="label[]" value="" class="form-control"> </td>'+
            '<td><input type="text" name="link[]" value="" class="form-control"></td>'+
            '<td><a href="#" class="add remove"><i class="icon-remove2"></i></a></td>'+
            '</tr>';
        $('tbody').append(tr);
      };

      $('body').delegate('.remove','click',function(){
          $(this).parent().parent().remove();
      });
    </script>

@endsection
