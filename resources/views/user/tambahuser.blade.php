@extends('master.master')


@section('body')
  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Dashboard</li>
    </ul>
  </div>
  <script type="text/javascript">
      $(function() {
        $("#q").autocomplete({
          source: "{{ URL('pegawai/autocompletekontrak') }}",
          minlength: 2,
          select: function(event, ui) {
            $("q").html(ui.item.value);
            onchange= this.form.submit()
          }
        });
      });
  </script>

  <!-- Table with checkboxes -->
  <form action="{{route('insert.user')}}" role="form" class="form-horizontal form-separate" method="post">
    {{ csrf_field() }}
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="block-inner text-danger">
              <h6 class="heading-hr">Tambah Pengguna</h6>
            </div>
            <div class="table-responsive">
                <div class="panel-heading" style="background:#2179cc">
                  <h6 class="panel-title" style=" color:#fff"><i class="icon-menu2"></i>Data Login</h6>
                </div>
                <table width="100%" class="table">
                    <tbody>
                      <tr>
                        <td>

                          <div class="col-md-12">
                            <div class="col-md-6">
                              <div class="form-group">
          						            <label class="col-md-2 control-label">Username:</label>
          						            <div class="col-md-10">
          						            	<input type="text" placeholder="Username" class="form-control" name="username">
          						            </div>
          						        </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
          						            <label class="col-md-2 control-label">Email:</label>
          						            <div class="col-md-10">
          						            	<input type="text" placeholder="Username" class="form-control" name="email">
          						            </div>
          						        </div>
                            </div>
                          </div>

                        </td>
                      </tr>
                      <tr>
                        <td>

                          <div class="col-md-12">
                            <div class="col-md-6">
                              <div class="form-group">
                                  <label class="col-md-2 control-label">User Group:</label>
                                  <div class="col-md-10">
                                     <select data-placeholder="usergroup" class="select-full" tabindex="2" name="usergroup">
                                        <option value=""></option>
                                        @foreach ($usergroup as $ug)
                                          <option value="{{$ug->ug_usergroup}}">{{$ug->ug_usergroup}}</option>
                                        @endforeach
                                      </select>
                                  </div>
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
          						            <label class="col-md-2 control-label">Password:</label>
          						            <div class="col-md-10">
          						            	<input type="text" placeholder="Username" class="form-control" name="password">
          						            </div>
          						        </div>
                            </div>
                          </div>

                        </td>
                      </tr>

                    </tbody>
                  </table>

                  <div class="panel-heading" style="background:#2179cc">
                    <h6 class="panel-title" style=" color:#fff"><i class="icon-menu2"></i>Data Pengguna</h6>
                  </div>
                  <table width="100%" class="table">
                      <tbody>
                        <tr>
                          <td>

                            <div class="col-md-12">
                              <div class="col-md-6">
                                <div class="form-group">
            						            <label class="col-md-2 control-label">Nama Depan:</label>
            						            <div class="col-md-10">
            						            	<input type="text" placeholder="nama depan" class="form-control" name="name">
            						            </div>
            						        </div>
                              </div>

                              <div class="col-md-6">
                                <div class="form-group">
            						            <label class="col-md-2 control-label">Nama Belakang:</label>
            						            <div class="col-md-10">
            						            	<input type="text" placeholder="nama belakang" class="form-control" name="last_name">
            						            </div>
            						        </div>
                              </div>
                            </div>

                          </td>
                        </tr>

                        <tr>
                          <td>

                            <div class="col-md-12">
                              <div class="col-md-6">
                                <div class="form-group">
            						            <label class="col-md-2 control-label">Jenis Identitas:</label>
            						            <div class="col-md-10">
            		                       <select data-placeholder="Pilih identitas" class="select-full" tabindex="2" name="jenis_identitas">
            		                          <option value=""></option>
            		                          <option value="KTP">KTP</option>
            		                          <option value="SIM">SIM</option>
            		                          <option value="DLL">Dan Lain Lain</option>
            		                        </select>
            						            </div>
            						        </div>
                              </div>

                              <div class="col-md-6">
                                <div class="form-group">
            						            <label class="col-md-2 control-label">Nomor Identitas:</label>
            						            <div class="col-md-10">
            						            	<input type="text" placeholder="nomor identitas" class="form-control" name="no_identitas">
            						            </div>
            						        </div>
                              </div>
                            </div>

                          </td>
                        </tr>

                        <tr>
                          <td>

                            <div class="col-md-12">
                              <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Jenis Kelamin:</label>
                                    <div class="col-md-10">
                                       <select data-placeholder="jenis kelamin" class="select-full" tabindex="2" name="jeniskelamin">
                                          <option value=""></option>
                                          <option value="Laki-Laki">Laki-Laki</option>
                                          <option value="Perempuan">Perempuan</option>

                                        </select>
                                    </div>
                                </div>
                              </div>

                              <div class="col-md-6">
                                <div class="form-group">
            						            <label class="col-md-2 control-label">Tanggal Lahir:</label>
            						            <div class="col-md-10">
            						            	<input type="date" placeholder="tanggal lahir" class="form-control" name="tanggallahir">
            						            </div>
            						        </div>
                              </div>
                            </div>

                          </td>
                        </tr>

                        <tr>
                          <td>

                            <div class="col-md-12">
                              <div class="col-md-6">
                                <div class="form-group">
            						            <label class="col-md-2 control-label">Negara:</label>
            						            <div class="col-md-10">
            						            	<input type="text" placeholder="negara" class="form-control" name="negara">
            						            </div>
            						        </div>
                              </div>

                              <div class="col-md-6">
                                <div class="form-group">
            						            <label class="col-md-2 control-label">Provinsi:</label>
            						            <div class="col-md-10">
            						            	<input type="text" placeholder="provinsi" class="form-control" name="provinsi">
            						            </div>
            						        </div>
                              </div>
                            </div>

                          </td>
                        </tr>

                        <tr>
                          <td>

                            <div class="col-md-12">
                              <div class="col-md-6">
                                <div class="form-group">
            						            <label class="col-md-2 control-label">Kota:</label>
            						            <div class="col-md-10">
            						            	<input type="text" placeholder="kota" class="form-control" name="kota">
            						            </div>
            						        </div>
                              </div>

                              <div class="col-md-6">
                                <div class="form-group">
            						            <label class="col-md-2 control-label">Alamat:</label>
            						            <div class="col-md-10">
                                      <textarea rows="5" cols="5" placeholder="Alamat..." class="elastic form-control" name="alamat"></textarea>
            						            </div>
            						        </div>
                              </div>
                            </div>

                          </td>
                        </tr>

                        <tr>
                          <td>

                            <div class="col-md-12">
                              <div class="col-md-6">
                                <div class="form-group">
            						            <label class="col-md-2 control-label">Kode Pos:</label>
            						            <div class="col-md-10">
            						            	<input type="text" placeholder="kode pos" class="form-control" name="kodepos">
            						            </div>
            						        </div>
                              </div>

                              <div class="col-md-6">
                                <div class="form-group">
            						            <label class="col-md-2 control-label">Nomor Handphone:</label>
            						            <div class="col-md-10">
            						            	<input type="text" placeholder="No. Hp" class="form-control" name="hp">
            						            </div>
            						        </div>
                              </div>
                            </div>

                          </td>
                        </tr>
                          <tr>
                            <td>
                              <div class="form-actions text-center">
                                <a href="{{url('/agama')}}" class="btn btn-success">Kembali</a>
                                <input type="reset" value="Reset" class="btn btn-info">
                                <button class="btn btn-danger" type="submit">Simpan</button>
                              </div>
                            </td>
                        </tr>
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
  </form>



@endsection
