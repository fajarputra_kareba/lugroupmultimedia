@extends('master.master')


@section('body')
  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Dashboard</li>
    </ul>
  </div>

    <form action="#" role="form" class="panel-filter">
      <div class="panel panel-default">
          <div class="panel-heading">
              <h6 class="panel-title">Filter Data</h6>
              <div class="panel-icons-group"> <a href="#" data-panel="collapse" class="btn btn-link btn-icon"><i class="icon-arrow-up9"></i></a></div>
          </div>
          <div class="panel-body">
              <div class="form-group">
                  <div class="row">
                      <div class="col-md-6">
                          <label>Nama Libur</label>
                          <input name="nama" class="form-control" type="text" value="" id="">
                      </div>
                      <div class="col-md-6">
                          <label>Per Bulan</label>
                          <div class="row">
                              <div class="col-md-8">
                                <select data-placeholder="Pilih Bulan" class="select-full" tabindex="2" name="bulan">
                                      <option value=""></option>
                                      <option value="1">Januari</option>
                                      <option value="2">Februari</option>
                                      <option value="3">Maret</option>
                                      <option value="4">April</option>
                                      <option value="5">Mei</option>
                                      <option value="6">Juni</option>
                                      <option value="7">Juli</option>
                                      <option value="8">Agustus</option>
                                      <option value="9">September</option>
                                      <option value="10">Oktober</option>
                                      <option value="11">November</option>
                                      <option value="12">Desember</option>
                                  </select>
                                </div>
                              <div class="col-md-4">
                                <select data-placeholder="Pilih tahun" class="select-full" tabindex="2" name="tahun">
                                      <option value=""></option>
                                      <option value="2018">2018</option>
                                      <option value="2017">2017</option>
                                      <option value="2016">2016</option>
                                      <option value="2015">2015</option>
                                      <option value="2014">2014</option>
                                  </select>
                            </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="form-actions text-center">
                  <input type="submit" value="Reset" class="btn btn-danger btn-filter-reset">
                  <input type="submit" value="Cari" class="btn btn-info btn-filter">
              </div>
          </div>
      </div>
  </form>

  <div class="panel panel-default">
    <div class="panel-body">
        <div class="block-inner text-danger">
            <h6 class="heading-hr">DATA KELUARGA
              <div class="pull-right">
                    <a data-toggle="modal" role="button" href="{{url('/kallibur/createkallibur')}}" class="btn btn-xs btn-success"><i class="icon-file-plus"></i>Tambah Data</a>
              </div>
                <small class="display-block">PT Ilugroup Multimedia</small>
            </h6>
        </div>
        <!-- Form modal -->
			<div id="form_modal" class="modal fade" tabindex="-1" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title"><i class="icon-paragraph-justify2"></i> Tambah Data</h4>
						</div>

						<!-- Form inside modal -->
						<form action="{{url('/keluarga/insertkeluarga')}}" role="form" method="post">
              {{ csrf_field() }}
							<div class="modal-body with-padding">
								<div class="block-inner text-danger">
									<h6 class="heading-hr">Tambah Data Keluarga</h6>
								</div>
                <div class="form-group">
									<div class="row">
									<div class="col-sm-6">
										<label>Nama</label>
										<input type="text" placeholder="Keluarga" class="form-control" name="keluarga">
									</div>
                </div>
								</div>

							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-primary">Simpan</button>
							</div>

						</form>
					</div>
				</div>
			</div>
			<!-- /form modal -->

        <div class="table-responsive pre-scrollable stn-table">
            <form id="checkboxForm" method="post" name="checkboxForm" action="">
                <table width="100%" class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th width="50">No</th>
                            <th class="text-center">Hari Libur</th>
                            <th class="text-center">Tanggal Libur</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                      @php
                        $no = 0;
                      @endphp
                      @foreach ($klibur as $kl)
                        @php
                          $no++
                        @endphp
                        <tr id="row-1" class="removeRow1">
                            <td class="text-center">{{$no}}</td>
                            <td class="text-center">{{$kl->kl_title}}</td>
                            <td class="text-center">{{$kl->kl_tgl_start}}</td>
                            <td class="text-center">
                              <form class="" action="{{ route('destroykallibur', $kl->kl_id) }}" method="post">
                                  {{ csrf_field() }}
                                  {{ method_field('Delete') }}
                                  <button type="submit" name="submit" class="btn btn-default btn-xs btn-icon tip" data-original-title="Remove"><i class="icon-remove2"></i></button>
                                  <a data-toggle="modal" href="#ubah-{{$kl->kl_id}}"><button type="button" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Detail"><i class="icon-eye"></i></button></a>
                                  <a data-toggle="modal" href="#{{$kl->kl_id}}"><button type="button" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Ubah"><i class="icon-pencil"></i></button></a>
                              </form>
                            </td>
                        </tr>
                        <!-- Form modal -->
                			<div id="{{$kl->kl_id}}" class="modal fade" tabindex="-1" role="dialog">
                				<div class="modal-dialog">
                					<div class="modal-content">
                						<div class="modal-header">
                							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                							<h4 class="modal-title"><i class="icon-paragraph-justify2"></i> Update Data</h4>
                						</div>

                						<!-- Form inside modal -->
                						<form action="{{route('updatekallibur', $kl->kl_id)}}" role="form" method="post">
                              {{ csrf_field() }}
                              {{ method_field('PATCH') }}
                							<div class="modal-body with-padding">
                								<div class="block-inner text-danger">
                									<h6 class="heading-hr">Update Data Kalender Perusahaan</h6>
                								</div>
                                <div class="form-group">
                									<div class="row">
                									<div class="col-sm-12">
                										<label>Title</label>
                										<input type="text" placeholder="Title" class="form-control" name="title" value="{{$kl->kl_title}}">
                									</div>
                                </div>
                								</div>

                                <div class="form-group">
                									<div class="row">
                									<div class="col-sm-6">
                										<label>Tanggal Mulai Libur</label>
                										<input type="text" name="start" placeholder="Mulai" class="form-control datepicker datepicker-proccesed" value="{{$kl->kl_tgl_start}}">
                									</div>

                									<div class="col-sm-6">
                										<label class="control-label">Tanggal Akhir Libur</label>
                										<input type="text" name="end" placeholder="Sampai" class="form-control datepicker datepicker-proccesed" value="{{$kl->kl_tgl_end}}">
                									</div>
                									</div>
                								</div>

                							</div>

                							<div class="modal-footer">
                								<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                								<button type="submit" class="btn btn-primary">Update</button>
                							</div>

                						</form>
                					</div>
                				</div>
                			</div>
                			<!-- /form modal -->
                      <!-- Form modal -->
                    <div id="ubah-{{$kl->kl_id}}" class="modal fade" tabindex="-1" role="dialog">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title"><i class="icon-paragraph-justify2"></i> Update Data</h4>
                          </div>

                          <!-- Form inside modal -->
                          <form action="{{route('updatekallibur', $kl->kl_id)}}" role="form" method="post">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <div class="modal-body with-padding">
                              <div class="block-inner text-danger">
                                <h6 class="heading-hr">Update Data Kalender Perusahaan</h6>
                              </div>
                              <div class="form-group">
                                <div class="row">
                                <div class="col-sm-12">
                                  <label>Title</label>
                                  <input type="text" placeholder="Title" class="form-control" name="title" value="{{$kl->kl_title}}" disabled>
                                </div>
                              </div>
                              </div>

                              <div class="form-group">
                                <div class="row">
                                <div class="col-sm-6">
                                  <label>Tanggal Mulai Libur</label>
                                  <input type="text" name="start" placeholder="Mulai" class="form-control datepicker datepicker-proccesed" value="{{$kl->kl_tgl_start}}" disabled>
                                </div>

                                <div class="col-sm-6">
                                  <label class="control-label">Tanggal Akhir Libur</label>
                                  <input type="text" name="end" placeholder="Sampai" class="form-control datepicker datepicker-proccesed" value="{{$kl->kl_tgl_end}}" disabled>
                                </div>
                                </div>
                              </div>

                            </div>

                            <div class="modal-footer">
                              <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Update</button>
                            </div>

                          </form>
                        </div>
                      </div>
                    </div>
                    <!-- /form modal -->
                      @endforeach
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>


@endsection
