@extends('master.master')


@section('body')
  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Dashboard</li>
    </ul>
  </div>

  <form action="{{route('insertkallibur')}}" class="form-horizontal form-separate" id="formSubmit" method="post" accept-charset="utf-8">
    {{ csrf_field() }}
    <div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="block-inner text-danger">
                    <h6 class="heading-hr">Tambah Kalender Libur</h6>
                </div>
                <div class="table-responsive">
                    <div class="panel-heading" style="background:#2179cc">
                            <h6 class="panel-title" style=" color:#fff"><i class="icon-menu2"></i>Data Kalender Libur</h6>
                        </div><table width="100%" class="table">

                        <tbody><tr>
                            <td>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="col-sm-3 col-md-4 control-label">Title</label>
                                            <div class="col-sm-9 col-md-8">
                                              <input name="title" class="form-control" maxlength="255" type="text">
                                            </div>
                                          </div>
                                        <div class="col-md-6">
                                            <label class="col-sm-3 col-md-4 control-label">Tanggal Libur</label>
                                            <div class="col-sm-4 col-md-4">
                                              <input class="form-control datepicker datepicker-proccesed" placeholder="Dari" type="text" name="start">
                                            </div>
                                            <div class="col-sm-4 col-md-4">
                                              <input class="form-control datepicker tip datepicker-proccesed" placeholder="Sampai" data-toggle="tooltip" title="" type="text" data-original-title="Isikan sama dengan tanggal mulai jika hanya 1 hari" name="end">
                                              </div>
                                          </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-actions text-center">
                                    <a href="{{route('kallibur')}}" class="btn btn-success">Kembali</a>
                                    <input type="reset" value="Reset" class="btn btn-info">
                                    <button class="btn btn-danger" data-toggle="modal" data-target="#add" type="submit">
                                        Simpan
                                      </button>
                                </div>
                            </td>
                        </tr>
                    </tbody></table>
                </div>
            </div>
        </div>
    </div>
</div>
</form>


@endsection
