@extends('master.master')


@section('body')
  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Dashboard</li>
    </ul>
  </div>

<center>
        <h1>{{ $title }}</h1>
        <p>{{ $content }}</p>
        @foreach ( $users as $user )
        <tr>
            <h3>Nama : {{ $user->name }}</h3>
        </tr>

        <table>
            <thead>
                <tr>
                    <th>Kendaraan</th>
                    <th>Jenis Kendaraan</th>
                    <th>Buatan</th>
                </tr>
            </thead>
            <tbody>
                @foreach($user->skill as $s)
                    <tr>
                        <td>{{ $s->skill }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @endforeach
    </center>
@endsection
