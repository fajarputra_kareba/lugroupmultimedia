

@extends('master.master')


@section('body')
  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Dashboard</li>
    </ul>
  </div>
  <script type="text/javascript">
      $(function() {
        $("#q").autocomplete({
          source: "{{ URL('pegawai/autocompletekontrak') }}",
          minlength: 2,
          select: function(event, ui) {
            $("q").html(ui.item.value);
            onchange= this.form.submit()
          }
        });
      });
  </script>
  <!-- /breadcrumbs line -->
  <!-- Alert -->
  <!-- <div class="alert alert-warning fade in block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
  </div> -->
  <!-- /alert -->
  <!-- Table with checkboxes -->
  <form action="{{route('absensi.filter_rekap')}}" role="form" class="panel-filter" method="post">
    {{ csrf_field() }}
  <div class="panel panel-default">
  <div class="panel-heading">
    <h6 class="panel-title">Filter Data</h6>
    <div class="panel-icons-group"> <a href="#" data-panel="collapse" class="btn btn-link btn-icon"><i class="icon-arrow-up9"></i></a></div>
  </div>
  <div class="panel-body">
    <div class="form-group">
        <div class="row">
            <div class="col-md-6">
              <label>Nama Pegawai:</label>
                <select data-placeholder="Nama Pegawai" class="select-full" tabindex="2" name="name">
                  <option value=""></option>
                  @foreach ($pegawai as $p)
                      <option value="{{ $p->id }}">{{ $p->name }}</option>
                  @endforeach
                </select>
            </div>
        </div>
    </div>

    <div class="form-actions text-center">
        <input type="reset" value="Reset" class="btn btn-danger btn-filter-reset">
        <input type="submit" value="Cari" class="btn btn-info btn-filter">
    </div>
  </div>
  </div>
</form>
<div class="panel panel-default">
<div class="panel-body">
<div class="block-inner text-danger">
    <h6 class="heading-hr">DATA IZIN PEGAWAI
      <div class="pull-right">
            <a href="{{route('createizin')}}">
              <button class="btn btn-xs btn-success" type="button">
                  <i class="icon-file-plus"></i>Tambah Data
              </button>
            </a>
            </div>
        <small class="display-block">PT Ilugroup Multimedia Indonesia -</small>
    </h6>
</div>
<div class="table-responsive pre-scrollable stn-table">
    <form id="checkboxForm" method="post" name="checkboxForm" action="">
        <table width="100%" class="table table-hover table-bordered">
            <thead>
                <tr>
                    <th width="50">No</th>
                    <th>Nama Pegawai</th>
                    <th>Departemen</th>
                    <th>Jabatan</th>
                    <th>Jenis Izin</th>
                    <th>Tanggal Awal</th>
                    <th>Tanggal Akhir</th>
                    <th>Status</th>
                    <th width="150">Aksi</th>
                </tr>
            </thead>
            <tbody>
              @if($izin->count())
              @php
                $no = $izin->firstItem() - 1;
              @endphp
              @foreach ($izin as $iz)
                @php
                  $no++;
                    $hari = array ( 1 =>    'Senin',
                      'Selasa',
                      'Rabu',
                      'Kamis',
                      'Jumat',
                      'Sabtu',
                      'Minggu'
                    );
                    $bulan = array (1 =>   'Januari',
                        'Februari',
                        'Maret',
                        'April',
                        'Mei',
                        'Juni',
                        'Juli',
                        'Agustus',
                        'September',
                        'Oktober',
                        'November',
                        'Desember'
                      );

                      $split = date("m", strtotime($iz->izin_date_start));
                      $bulanstart = $bulan[(int)$split[1]];
                      $year = date("Y", strtotime($iz->izin_date_start));
                      $day = date("d", strtotime($iz->izin_date_start));
                      $num = date("N", strtotime($iz->izin_date_start));
                      $split1 = date("m", strtotime($iz->izin_date_end));
                      $bulanend = $bulan[(int)$split[1]];
                      $year1 = date("Y", strtotime($iz->izin_date_end));
                      $day1 = date("d", strtotime($iz->izin_date_end));
                      $num1 = date("N", strtotime($iz->izin_date_end));
                @endphp
                <tr id="row-1">
                    <td class="text-center">{{$no}}</td>
                    <td class="text-center"> {{$iz->name}}</td>
                    <td class="text-center">{{$iz->department}}</td>
                    <td class="text-center">{{$iz->jabatan}}</td>
                    <td class="text-center">{{$iz->ji_jenis}}</td>
                    <td class="text-center">{{ $hari[$num].", ".$day." ".$bulanstart." ".$year }}</td>
                    <td class="text-center">{{ $hari[$num1].", ".$day1." ".$bulanend." ".$year1 }}</td>
                    <td class="text-center" id="target-change-statuses1">{{$iz->izin_status}}</td>
                    <td class="text-center">
                      <form class="" action="{{ route('destroyizin', $iz->izin_id) }}" method="post">
                          {{ csrf_field() }}
                          {{ method_field('Delete') }}
                          <button type="submit" name="submit" class="btn btn-default btn-xs btn-icon tip" data-original-title="Remove"><i class="icon-remove2"></i></button>
                          <a href="{{route('detailizin', $iz->izin_id)}}"><button type="button" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Detail"><i class="icon-file"></i></button></a>
                          <a href="{{route('editizin', $iz->izin_id)}}"><button type="button" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Ubah"><i class="icon-pencil"></i></button></a>
                      </form>
                    </td>
                  </tr>
              @endforeach
            @endif
              </tbody>
        </table>
    </form>
</div>
</div>

<script>
$(document).ready(function () {
$("#pagination-limit").bind("change", function () {
    window.location.href = $("#pagination-limit option:selected").data("url");
})
$(".c-pagination").each(function () {
    var aEle = $(this).find("a");
    if (aEle.length == 0) {
        var html = $(this).html();
        console.log()
        $(this).html("").append("<a></a>").find("a").html(html);
    }
})
})
</script></div>


                 <!-- /table with checkboxes -->

                  <script type="text/javascript">
                      $(document).ready(function () {


                          $('#master').on('click', function(e) {
                           if($(this).is(':checked',true))
                           {
                              $(".sub_chk").prop('checked', true);
                           } else {
                              $(".sub_chk").prop('checked',false);
                           }
                          });


                          $('.delete_all').on('click', function(e) {


                              var allVals = [];
                              $(".sub_chk:checked").each(function() {
                                  allVals.push($(this).attr('data-id'));
                              });


                              if(allVals.length <=0)
                              {
                                  alert("Please select row.");
                              }  else {


                                  var check = confirm("Are you sure you want to delete this row?");
                                  if(check == true){


                                      var join_selected_values = allVals.join(",");


                                      $.ajax({
                                          url: $(this).data('url'),
                                          type: 'DELETE',
                                          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                          data: 'ids='+join_selected_values,
                                          success: function (data) {
                                              if (data['success']) {
                                                  $(".sub_chk:checked").each(function() {
                                                      $(this).parents("tr").remove();
                                                  });
                                                  alert(data['success']);
                                              } else if (data['error']) {
                                                  alert(data['error']);
                                              } else {
                                                  alert('Whoops Something went wrong!!');
                                              }
                                          },
                                          error: function (data) {
                                              alert(data.responseText);
                                          }
                                      });


                                    $.each(allVals, function( index, value ) {
                                        $('table tr').filter("[data-row-id='" + value + "']").remove();
                                    });
                                  }
                              }
                          });


                          $('[data-toggle=confirmation]').confirmation({
                              rootSelector: '[data-toggle=confirmation]',
                              onConfirm: function (event, element) {
                                  element.trigger('confirm');
                              }
                          });


                          $(document).on('confirm', function (e) {
                              var ele = e.target;
                              e.preventDefault();


                              $.ajax({
                                  url: ele.href,
                                  type: 'DELETE',
                                  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                  success: function (data) {
                                      if (data['success']) {
                                          $("#" + data['tr']).slideUp("slow");
                                          alert(data['success']);
                                      } else if (data['error']) {
                                          alert(data['error']);
                                      } else {
                                          alert('Whoops Something went wrong!!');
                                      }
                                  },
                                  error: function (data) {
                                      alert(data.responseText);
                                  }
                              });


                              return false;
                          });
                      });
                  </script>
@endsection
