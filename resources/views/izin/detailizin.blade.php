@extends('master.master')


@section('body')
  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Dashboard</li>
    </ul>
  </div>

  <!-- Table with checkboxes -->
@foreach ($izin as $iz)
  <form action="{{route('updateizin', $iz->izin_id)}}" role="form" class="form-horizontal form-separate" method="post">
    {{ csrf_field() }}
    {{ method_field('PATCH') }}
  <div class="panel panel-default">
  <div class="panel-heading">
    <h6 class="panel-title">Update Data Izin</h6>
    <div class="panel-icons-group"> <a href="#" data-panel="collapse" class="btn btn-link btn-icon"><i class="icon-arrow-up9"></i></a></div>
  </div>
  <div class="panel-body">
    <div class="form-group">
      <div class="row">
          <div class="col-md-6">
              <div class="panel-heading" style="background:#2179cc; margin:10px;">
                  <h6 class="panel-title" style=" color:#fff"><i class="icon-menu2"></i>Data Pegawai</h6>
              </div>
              <input type="hidden" name="id" value="{{$iz->izin_id}}">
              <div class="form-group" style="margin:10px;">
  				       <label class="col-md-2 control-label">Pegawai:</label>
  				          <div class="col-md-10">
                      <select data-placeholder="Nama Pegawai" class="select-full" tabindex="2" name="user" disabled>
                        <option value=""></option>
                          @foreach ($users as $u)
                              <option @if ($u->id == $iz->izin_user_id) {{"selected"}} @endif value="{{ $u->id }}" >{{ $u->name }}</option>
                          @endforeach
                      </select>
  				           </div>
  				      </div>

                <div class="form-group" style="margin:10px;">
    				       <label class="col-md-2 control-label">Status:</label>
    				          <div class="col-md-10">
                        <select data-placeholder="Nama Pegawai" class="select-full" tabindex="2" name="status" disabled>
                          <option value=""></option>
                          <option @if ($iz->izin_status == "Pending") {{"selected"}} @endif value="{{ "Pending" }}">Pending</option>
                          <option @if ($iz->izin_status == "Dibatalkan") {{"selected"}} @endif value="{{ "Dibatalkan" }}">Dibatalkan</option>
                          <option @if ($iz->izin_status == "Disetujui") {{"selected"}} @endif value="{{ "Disetujui" }}">Disetujui</option>
                        </select>
    				           </div>
    				     </div>
            </div>

            <div class="col-md-6">
              <div class="panel-heading" style="background:#2179cc; margin:10px;">
                  <h6 class="panel-title" style=" color:#fff"><i class="icon-menu2"></i>Data Izin</h6>
              </div>
              <div class="form-group" style="margin:10px;">
  				       <label class="col-md-4 control-label">Jenis Izin:</label>
  				          <div class="col-md-8">
                      <select data-placeholder="Jenis Izin" class="select-full" tabindex="2" name="jenis" disabled>
                        <option value=""></option>
                        @foreach ($jenisizin as $jen)
                            <option @if ($jen->ji_id == $iz->izin_jenis_id) {{"selected"}} @endif value="{{ $jen->ji_id }}" >{{ $jen->ji_jenis }}</option>
                        @endforeach
                      </select>
  				           </div>
  				      </div>

              <div class="form-group" style="margin:10px;">
				          <label class="col-md-4 control-label">Tanggal Awal:</label>
				            <div class="col-md-8">
                      <input class="form-control" type="date" name="tglawal" value="{{$iz->izin_date_start}}" disabled>
				            </div>
				       </div>
              <div class="form-group" style="margin:10px;">
				          <label class="col-md-4 control-label">Tanggal Akhir:</label>
				            <div class="col-md-8">
                      <input class="form-control" type="date" name="tglakhir" value="{{$iz->izin_date_end}}" disabled>
				            </div>
				       </div>
        </div>
        <div class="col-md-12">
          <div class="form-group" style="margin:10px;">
             <label class="col-md-2 control-label">Keterangan Izin:</label>
             <div class="col-md-10">
               <textarea rows="5" cols="5" placeholder="Keterangan..." class="elastic form-control" name="keterangan" disabled>@if ($iz->izin_keterangan == '0'){{""}}@else{{$iz->izin_keterangan}}@endif</textarea>
             </div>
         </div>
          <div class="form-group" style="margin:10px;">
             <label class="col-md-2 control-label">Catatan Personalia:</label>
             <div class="col-md-10">
               <textarea rows="5" cols="5" placeholder="Keterangan..." class="elastic form-control" name="personalia" disabled>@if ($iz->izin_personalia_ket == '0'){{""}}@else{{$iz->izin_personalia_ket}}@endif</textarea>
             </div>
         </div>
          <div class="form-group" style="margin:10px;">
             <label class="col-md-2 control-label">Catatan General Manager:</label>
             <div class="col-md-10">
               <textarea rows="5" cols="5" placeholder="Keterangan..." class="elastic form-control" name="general" disabled>@if ($iz->izin_gm_ket == '0'){{""}}@else{{$iz->izin_gm_ket}}@endif</textarea>
             </div>
         </div>
        </div>
    </div>
   </div>
  </div>

  <div class="panel-body">
    <div class="form-group">
    <div class="form-actions text-center">
        <a href="{{route('izin')}}" type="submit" class="btn btn-success btn-filter-reset">Kembali</a>
    </div>
  </div>
  </div>
</div>
</form>
@endforeach

@endsection
