

@extends('master.master')


@section('body')
  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Dashboard</li>
    </ul>
  </div>
  <script type="text/javascript">
      $(function() {
        $("#q").autocomplete({
          source: "{{ URL('pegawai/autocompletekontrak') }}",
          minlength: 2,
          select: function(event, ui) {
            $("q").html(ui.item.value);
            onchange= this.form.submit()
          }
        });
      });
  </script>
  <!-- /breadcrumbs line -->
  <!-- Alert -->
  <!-- <div class="alert alert-warning fade in block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
  </div> -->
  <!-- /alert -->
  <!-- Table with checkboxes -->
<div class="panel panel-default">
<div class="panel-body">
<div class="block-inner text-danger">
    <h6 class="heading-hr">DATA JENIS IZIN
      <div class="pull-right">
            <a href="{{route('createjenisizin')}}">
              <button class="btn btn-xs btn-success" type="button">
                  <i class="icon-file-plus"></i>Tambah Data
              </button>
            </a>
            </div>
        <small class="display-block">PT Ilugroup Multimedia Indonesia -</small>
    </h6>
</div>
<div class="table-responsive pre-scrollable stn-table">
    <form id="checkboxForm" method="post" name="checkboxForm" action="">
        <table width="70%" class="table table-hover table-bordered col-md-8">
            <thead>
                <tr>
                    <th class="text-center" width="50">No</th>
                    <th class="text-center" width="200">Jenis Izin</th>
                    <th class="text-center" width="200">Kategori Izin</th>
                    <th class="text-center" width="200">Kode Izin</th>
                    <th class="text-center" width="100">Aksi</th>
                </tr>
            </thead>
            <tbody>
              @if($jenisizin->count())
              @php
                $no = $jenisizin->firstItem() - 1;
              @endphp
              @foreach ($jenisizin as $jizin)
                @php
                  $no++;
                @endphp
                <tr id="row-1">
                    <td class="text-center">{{$no}}</td>
                    <td class="text-center"> {{$jizin->ji_jenis}}</td>
                    <td class="text-center"> {{$jizin->ji_kategori}}</td>
                    <td class="text-center"> {{$jizin->ji_kode}}</td>
                    <td class="text-center">
                      <form class="" action="{{ route('destroyjenisizin', $jizin->ji_id) }}" method="post">
                          {{ csrf_field() }}
                          {{ method_field('Delete') }}
                          <button type="submit" name="submit" class="btn btn-default btn-xs btn-icon tip" data-original-title="Remove"><i class="icon-remove2"></i></button>
                          <a href="{{route('editjenisizin', $jizin->ji_id)}}"><button type="button" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Detail"><i class="icon-pencil"></i></button></a>
                      </form>
                    </td>
                  </tr>
              @endforeach
            @endif
              </tbody>
        </table>
    </form>
</div>
</div>

</div>


                 <!-- /table with checkboxes -->

                  <script type="text/javascript">
                      $(document).ready(function () {


                          $('#master').on('click', function(e) {
                           if($(this).is(':checked',true))
                           {
                              $(".sub_chk").prop('checked', true);
                           } else {
                              $(".sub_chk").prop('checked',false);
                           }
                          });


                          $('.delete_all').on('click', function(e) {


                              var allVals = [];
                              $(".sub_chk:checked").each(function() {
                                  allVals.push($(this).attr('data-id'));
                              });


                              if(allVals.length <=0)
                              {
                                  alert("Please select row.");
                              }  else {


                                  var check = confirm("Are you sure you want to delete this row?");
                                  if(check == true){


                                      var join_selected_values = allVals.join(",");


                                      $.ajax({
                                          url: $(this).data('url'),
                                          type: 'DELETE',
                                          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                          data: 'ids='+join_selected_values,
                                          success: function (data) {
                                              if (data['success']) {
                                                  $(".sub_chk:checked").each(function() {
                                                      $(this).parents("tr").remove();
                                                  });
                                                  alert(data['success']);
                                              } else if (data['error']) {
                                                  alert(data['error']);
                                              } else {
                                                  alert('Whoops Something went wrong!!');
                                              }
                                          },
                                          error: function (data) {
                                              alert(data.responseText);
                                          }
                                      });


                                    $.each(allVals, function( index, value ) {
                                        $('table tr').filter("[data-row-id='" + value + "']").remove();
                                    });
                                  }
                              }
                          });


                          $('[data-toggle=confirmation]').confirmation({
                              rootSelector: '[data-toggle=confirmation]',
                              onConfirm: function (event, element) {
                                  element.trigger('confirm');
                              }
                          });


                          $(document).on('confirm', function (e) {
                              var ele = e.target;
                              e.preventDefault();


                              $.ajax({
                                  url: ele.href,
                                  type: 'DELETE',
                                  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                  success: function (data) {
                                      if (data['success']) {
                                          $("#" + data['tr']).slideUp("slow");
                                          alert(data['success']);
                                      } else if (data['error']) {
                                          alert(data['error']);
                                      } else {
                                          alert('Whoops Something went wrong!!');
                                      }
                                  },
                                  error: function (data) {
                                      alert(data.responseText);
                                  }
                              });


                              return false;
                          });
                      });
                  </script>
@endsection
