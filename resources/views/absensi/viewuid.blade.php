@extends('master.master')

@section('body')

  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Modul</li>
    </ul>
  </div>
  <!-- /breadcrumbs line -->


  <!-- Table inside panel body -->
                <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
  			                <div class="panel-heading">
                          <h4 class="panel-title"> Data UID Pegawai
                            <small class="display-block">PT Ilugroup Multimeida Indonesia -</small>
                          </h4>

                        </div>
                        <form class="form-horizontal" action="{{ route('absensi.update') }}" method="post">
                          {{ csrf_field() }}
                          {{ method_field('PATCH') }}
  			                <div class="panel-body">
                        <div class="table-responsive">
                          <div class="pull-right">
                              <a data-toggle="modal" role="button" href="#form_tambah" class="btn btn-xs btn-success" style="margin-bottom:10px; margin-top:10px;"><i class="icon-file-plus"></i>Tambah Data</a>
                          </div>

  					                <table class="table table-bordered">
  					                    <thead class="text-center">
                                  <tr >
                                       <td rowspan="2"> #</td>
                                       <td rowspan="2"> Nama Pegawai</td>
                                       <td colspan="2"> Mesin</td>

                                   </tr>
                                   <tr>
                                       <td> Mesin 1</td>
                                       <td> Mesin 2</td>
                                   </tr>
  					                    </thead>
  					                    <tbody>

                                  @foreach ($uid as $key => $u)
                                    <tr>
  					                            <td>{{++$key}}</td>
  					                            <td class="text-center">
                                          <label>
                                              <input type="text" class="form-control" name="name[]" value="{{$u->name}}" disabled>
                                          </label>
                                          <input type="hidden" name="user[]" value="{{$u->data_user_id}}" class="styled" >
                                          <input type="hidden" name="id[]" value="{{$u->data_id}}" class="styled" >
                                        </td>
  					                            <td class="text-center">
                                          <label>
                                              <input type="number" class="form-control" name="uid1[]" value="{{$u->data_uid_mesin}}">
                                          </label>
                                        </td>
                                        <td class="text-center">
                                          <label>
                                              <input type="number" class="form-control" name="uid2[]" value="{{$u->data_uid_mesin2}}">
                                          </label>
                                        </td>
                                    </tr>
                                  @endforeach

  					                    </tbody>
  					                </table>
  				                </div>
  			                </div>
                        <div class="panel-body">
                          <div class="block-inner text-danger">
                              <div class="form-actions text-center">
                                  <input name="Button" type="button" onclick="history.go(-1);" class="btn btn-success" value="Kembali">
                                  <input type="reset" value="Reset" class="btn btn-info">
                                  <button class="btn btn-danger" type="submit">
                                    Simpan
                                  </button>
                              </div>
                          </div>
                      </div>
                      </form>
                      <!-- Form modal -->
                        <div id="form_tambah" class="modal fade" tabindex="-1" role="dialog">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title"><i class="icon-paragraph-justify2"></i> Data Absen</h4>
                              </div>

                              <!-- Form inside modal -->
                              <form action="{{ route('absensi.insert') }}" method="post">
                                {{ csrf_field() }}
                                <div class="modal-body with-padding">

                                  <div class="form-group">
                                    <label>Nama User:</label>
                                      <select data-placeholder="nama user..." class="select-full" tabindex="2" name="user[]">
                                        <option value=""></option>

                                        @foreach ($user as $u)
                                            <option value="{{$u->id}}">{{$u->name.' '.$u->nama_belakang}}</option>
                                        @endforeach

                                      </select>
                                  </div>

                                  <div class="form-group">
                                    <div class="row">
                                    <div class="col-sm-6">
                                      <label>Mesin 1</label>
                                      <input type="text" placeholder="uid" class="form-control" name="uid1[]">
                                    </div>

                                    <div class="col-sm-6">
                                      <label class="control-label">Mesin 2</label>
                                      <input type="text" placeholder="uid" class="form-control" name="uid2[]">
                                    </div>
                                    </div>
                                  </div>

                                </div>

                                <div class="modal-footer">
                                  <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                                  <button type="submit" class="btn btn-primary">Submit form</button>
                                </div>

                              </form>
                            </div>
                          </div>
                        </div>
                        <!-- /form modal -->

  				        </div>
                  </center>
                </div>

				        <!-- /table inside panel body -->
@endsection
