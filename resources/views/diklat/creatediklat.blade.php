@extends('master.master')


@section('body')
  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Dashboard</li>
    </ul>
  </div>

  <!-- Table with checkboxes -->
  <form action="{{route('insertdiklat')}}" role="form" class="form-horizontal form-separate" method="post">
    {{ csrf_field() }}
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="block-inner text-danger">
              <h6 class="heading-hr">Tambah Jenis Diklat</h6>
            </div>
            <div class="table-responsive">
                <div class="panel-heading" style="background:#2179cc">
                  <h6 class="panel-title" style=" color:#fff"><i class="icon-menu2"></i>Data Jenis Diklat</h6>
                </div>
                <table width="100%" class="table">
                    <tbody>
                      <tr>
                        <td>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-md-6">
                                <label for="ReligionName" class="col-sm-3 col-md-4 control-label">Nama</label>
                                <div class="col-sm-9 col-md-8">
                                  <input name="diklat" class="form-control" maxlength="255" type="text" >
                                </div>
                              </div>
                            </div>
                          </div>
                        </td>
                      </tr>
                        <tr>
                          <td>
                            <div class="form-actions text-center">
                              <a href="{{url('/diklat')}}" class="btn btn-success">Kembali</a>
                              <input type="reset" value="Reset" class="btn btn-info">
                              <button class="btn btn-danger" type="submit">Simpan</button>
                            </div>
                          </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
  </form>


@endsection
