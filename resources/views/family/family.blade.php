@extends('master.master')

@section('body')

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
  <ul class="breadcrumb">
    <li style="color:#fff"><a href="index.html">Home</a></li>
    <li class="active">Kepegawaian</li>
  </ul>
</div>
<!-- /breadcrumbs line -->
<!-- Alert -->
<!-- <div class="alert alert-warning fade in block">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
</div> -->
<!-- /alert -->
<!-- Simple contact form -->
    <form action="{{ route('family.insert') }}" role="form" method="post">
          {{ csrf_field() }}
          <div class="panel panel-default">
          <div class="panel-heading"><h6 class="panel-title"><i class="icon-pencil3"></i> Form Pengisian Data Keluarga</h6></div>
          <div class="panel-body">
          <div class="form-group">
            <div class="row">
              <div class="col-md-6" style="padding:10px;">
									<label>Nama:</label>
                    <select name="id" data-placeholder="Choose an option..." class="form-control" tabindex="2">
                      <option value=""></option>
                      @foreach($users as $user)
                        <option value="{{ $user->id }}" {{ $selectedUser == $user->id ? 'selected="selected"' : '' }}>{{ $user->name }}</option>
                      @endforeach
                    </select>
							</div>

              <div class="col-md-6" style="padding:10px;">
                <label>Nama Keluarga:</label>
                    <input type="text" name="keluarga" placeholder="Nama Keluarga" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Tempat Lahir:</label>
                    <input type="text" name="tempat" placeholder="Tempat Lahir" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Tanggal Lahir:</label>
                    <input type="date" name="tanggal" placeholder="Tanggal Lahir" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Jenis Kelamin:</label>
                    <input type="text" name="gender" placeholder="Jenis Kelamin" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Pendidikan Terakhir:</label>
                    <input type="text" name="pendidikan" placeholder="Pendidikan Terakhir" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Pekerjaan:</label>
                    <input type="text" name="pekerjaan" placeholder="Pekerjaan" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Agama:</label>
                    <input type="text" name="agama" placeholder="Agama" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Status Pernikahan:</label>
                    <input type="text" name="nikah" placeholder="Status Pernikahan" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Status Hidup:</label>
                    <input type="text" name="hidup" placeholder="Status Hidup" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Status Anak:</label>
                    <input type="text" name="anak" placeholder="Status Anak" class="form-control">
              </div>

            </div>
          </div>



          <div class="form-actions text-center">
            <input type="reset" value="Cancel" class="btn btn-danger">
            <input type="submit" value="Insert" class="btn btn-primary">
          </div>

        </div>
      </div>
    </form>
    <!-- /simple contact form -->
@endsection
