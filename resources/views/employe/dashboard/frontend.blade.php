@extends('master.master')


@section('body')

			<!-- Breadcrumbs line -->
			<div class="breadcrumb-line">
				<ul class="breadcrumb">
					<li><a href="index.html">Home</a></li>
					<li class="active">Dashboard</li>
				</ul>

				<div class="visible-xs breadcrumb-toggle">
					<a class="btn btn-link btn-lg btn-icon" data-toggle="collapse" data-target=".breadcrumb-buttons"><i class="icon-menu2"></i></a>
				</div>
			</div>
			<!-- /breadcrumbs line -->
            <!-- Questions and contact -->
            <div class="row">
            	<div class="col-md-6">

                	<!-- Questions -->
                  <div class="block">
                    <h6 class="heading-hr"><i class="icon-file"></i> Info Update</h6>
                    <ul class="media-list">
                      <div class="alert alert-block alert-success fade in">
        								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        								<h6><i class="icon-command"></i> Oh snap! You got an error!</h6>
        								<hr>
        								<p>Change this and that and try again. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum.</p>
        								<div class="text-left">
        									<a class="btn btn-info" href="#"><i class="icon-link"></i> Take action</a>
        									<a class="btn btn-warning" href="#"><i class="icon-link2"></i> Or do this</a>
        								</div>
        							</div>

                    </ul>
                      </div>
					<!-- Questions -->

            	</div>

            	<div class="col-md-6">

                <!-- Recent activity -->
            <div class="block">
              <h6 class="heading-hr"><i class="icon-file"></i> Daftar Aktifitas</h6>
              <ul class="media-list">
                <li class="media">
                  <a class="pull-left" href="#">
                    <img class="media-object" src="http://placehold.it/300" alt="">
                  </a>
                  <div class="media-body">
                    <div class="clearfix">
                      <a href="#" class="media-heading">Eugene Kopyov</a>
                      <span class="media-notice">December 10, 2013 / 10:20 pm</span>
                    </div>
                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.
                  </div>
                </li>

                <li class="media">
                  <a class="pull-left" href="#">
                    <img class="media-object" src="http://placehold.it/300" alt="">
                  </a>
                  <div class="media-body">
                    <div class="clearfix">
                      <a href="#" class="media-heading">Martin Wolf</a>
                      <span class="media-notice">December 12, 2013 / 10:14 pm</span>
                    </div>
                    Cras tempus pretium ligula, quis viverra purus eleifend et.
                  </div>
                </li>

                <li class="media">
                  <a class="pull-left" href="#">
                    <img class="media-object" src="http://placehold.it/300" alt="">
                  </a>
                  <div class="media-body">
                    <div class="clearfix">
                      <a href="#" class="media-heading">Duncan McMart</a>
                      <span class="media-notice">January 3, 2014 / 12:14 pm</span>
                    </div>
                    Quisque dignissim nibh nec massa egestas interdum. Proin congue vulputate velit, sodales mattis neque tempor a.
                  </div>
                </li>

              </ul>
                </div>
                <!-- /recent activity -->

            	</div>

            </div>
            <!-- /questions and contact -->

	        <!-- Tasks table -->
	        <div class="block">
	        	<h6 class="heading-hr"><i class="icon-grid"></i> Data Pegawai</h6>
	            <div class="datatable-tasks">
	                <table class="table table-bordered">
	                    <thead>
	                        <tr>
	                            <th class="task-tools text-center">#</th>
	                            <th class="task-priority">Nama</th>
	                            <th class="task-date-added">Department</th>
	                            <th class="task-progress">Jabatan</th>
	                            <th class="task-priority">Status</th>
	                            <th class="task-priority">Gender</th>
	                        </tr>
	                    </thead>
	                    <tbody>
                          @foreach ($users as $key => $user)
                            <tr>
                                <td class="text-center">{{ ++$key }}</td>
                                <td class="task-desc">{{ $user->name }}</td>
  	                            <td class="text-center">{{ $user->pos_department }}</td>
  	                            <td class="text-center">{{ $user->pos_jabatan }}</td>
  	                            <td class="text-center">{{ $user->status }}</td>
  	                            <td class="text-center">{{ $user->gender }}</td>
  	                        </tr>
                          @endforeach

	                    </tbody>
	                </table>
	            </div>
	        </div>
	        <!-- /tasks table -->

@endsection
