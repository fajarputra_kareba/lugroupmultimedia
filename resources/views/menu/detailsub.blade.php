@extends('master.master')

@section('body')

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
  <ul class="breadcrumb">
    <li style="color:#fff"><a href="index.html">Home</a></li>
    <li class="active">Sub Menu</li>
  </ul>
</div>
<!-- /breadcrumbs line -->
<!-- Alert -->
<!-- <div class="alert alert-warning fade in block">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
</div> -->
<!-- /alert -->
<!-- Simple contact form -->
@foreach ($submenu as $sub)
    <form action="" role="form" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          {{ method_field('PATCH') }}
          <div class="panel panel-default">
          <div class="panel-heading"><h6 class="panel-title"><i class="icon-pencil3"></i> Detail Submenu </h6></div>
          <div class="panel-body">

            <div class="form-group">
              <div class="row">
                <div class="col-md-6" style="padding:10px;">
  									<label>Menu Parent:</label>
                      <select name="menu" data-placeholder="Choose an option..." class="form-control" tabindex="2" disabled>
                        <option value=""></option>
                        @foreach($menu as $me)
                          <option value="{{ $me->m_id }}"
                            @if($sub->sub_m_id == $me->m_id)

                              {{ "selected" }}

                            @endif
                          >{{ $me->m_menu }}</option>
                        @endforeach
                      </select>
  								</div>

                <div class="col-md-6" style="padding:10px;">
                  <label>Label Submenu:</label>
                      <input type="text" name="label" value="{{$sub->sub_label}}" class="form-control" disabled>
                </div>

                <div class="col-md-6" style="padding:10px;">
                  <label>Link Submenu:</label>
                      <input type="text" name="link" value="{{$sub->sub_link}}" class="form-control" disabled>
                </div>


              </div>
            </div>

          <div class="form-actions text-right">
            <a href="{{ route('submenu') }}" class="btn btn-danger">Kembali</a>
          </div>

        </div>
      </div>
    </form>
    <!-- /simple contact form -->
  @endforeach
@endsection
