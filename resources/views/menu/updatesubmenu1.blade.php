@extends('master.master')

@section('body')

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
  <ul class="breadcrumb">
    <li style="color:#fff"><a href="index.html">Home</a></li>
    <li class="active">Sub Menu</li>
  </ul>
</div>
<!-- /breadcrumbs line -->
<!-- Alert -->
<!-- <div class="alert alert-warning fade in block">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
</div> -->
<!-- /alert -->
<!-- Simple contact form -->
@foreach ($submenu1 as $sub1)
    <form action="{{ route('submenu1.update', $sub1->sub1_id) }}" role="form" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          {{ method_field('PATCH') }}
          <div class="panel panel-default">
          <div class="panel-heading"><h6 class="panel-title"><i class="icon-pencil3"></i> Form Pengisian Submenu </h6></div>
          <div class="panel-body">

            <div class="form-group">
              <div class="row">
                <div class="col-md-6" style="padding:10px;">
  									<label>Menu Parent:</label>
                      <select name="submenu" data-placeholder="Choose an option..." class="form-control" tabindex="2">
                        <option value=""></option>
                        @foreach($submenu as $sub)
                          <option value="{{ $sub->sub_id }}"
                            @if($sub1->sub1_sub_id == $sub->sub_id)

                              {{ "selected" }}

                            @endif
                          >{{$sub->m_menu.' - '.$sub->sub_label }}</option>
                        @endforeach
                      </select>
  								</div>

                <div class="col-md-6" style="padding:10px;">
                  <label>Label Submenu:</label>
                      <input type="text" name="label1" value="{{$sub1->sub1_label}}" class="form-control">
                </div>

                <div class="col-md-6" style="padding:10px;">
                  <label>Link Submenu:</label>
                      <input type="text" name="link1" value="{{$sub1->sub1_link}}" class="form-control">
                </div>


              </div>
            </div>

          <div class="form-actions text-right">
            <a href="{{ route('submenu') }}" class="btn btn-danger">Cancel</a>
            <input type="submit" value="update" class="btn btn-primary">
          </div>

        </div>
      </div>
    </form>
    <!-- /simple contact form -->
  @endforeach
@endsection
