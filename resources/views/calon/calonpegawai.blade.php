@extends('master.master')

@section('body')

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
  <ul class="breadcrumb">
    <li style="color:#fff"><a href="index.html">Home</a></li>
    <li class="active">Kepegawaian</li>
  </ul>
</div>
<!-- /breadcrumbs line -->
<!-- Alert -->
<!-- <div class="alert alert-warning fade in block">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
</div> -->
<!-- /alert -->
<!-- Simple contact form -->
    <form action="{{ route('calon.insert') }}" role="form" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="panel panel-default">
          <div class="panel-heading"><h6 class="panel-title"><i class="icon-pencil3"></i> Form Pengisian Data </h6></div>
          <div class="panel-body">

          <div class="form-group">
            <div class="row">
              <div class="col-md-6" style="padding:10px;">
                <label>Nama:</label>
                    <input type="text" name="name" placeholder="Nama Lengkap" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Telepon:</label>
                    <input type="text" name="telepon" placeholder="Telepon" class="form-control">
              </div>
              <div class="col-md-6" style="padding:10px;">
                <label>Alamat:</label>
                    <input type="text" name="alamat" placeholder="Alamat" class="form-control">
              </div>
              <div class="col-md-6" style="padding:10px;">
                <label>Gender:</label>
                    <input type="text" name="gender" placeholder="gender" class="form-control">
              </div>
              <div class="col-md-6" style="padding:10px;">
                <label>File CV:</label>
                    <input type="file" name="filecv" class="form-control">
              </div>


              <div class="col-md-6" style="padding:10px;">
                <label>Photo:</label>
                    <input type="file" name="gambar" placeholder="Photo" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Skill:</label>
                    <input type="text" name="skill" placeholder="Skill" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Pengalaman:</label>
                    <input type="text" name="pengalaman" placeholder="password" class="form-control">
              </div>

            </div>
          </div>

          <div class="form-actions text-right">
            <input type="reset" value="Cancel" class="btn btn-danger">
            <input type="submit" value="Insert" class="btn btn-primary">
          </div>

        </div>
      </div>
    </form>
    <!-- /simple contact form -->
@endsection
