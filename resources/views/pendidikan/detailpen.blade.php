@extends('master.master')

@section('body')

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
  <ul class="breadcrumb">
    <li style="color:#fff"><a href="index.html">Home</a></li>
    <li class="active">Kepegawaian</li>
  </ul>
</div>
<!-- /breadcrumbs line -->
<!-- Separated form outside panel -->
@foreach ($riwayat as $r)
          <div class="panel-body">
            <form class="form-horizontal form-separate" action="{{ route( 'pegawai.detail', $r->user_id )}}" role="form">
  						<h6><i class="icon-page-break"></i> Detail Pendidikan</h6>

                  <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4 control-label label-required">Sekolah/Universitas:</label>
                        <div class="col-md-8">
                          <input type="text" value="{{ $r->sekolah }}" class="form-control" disabled>
                        </div>
                    </div>

                  </div>


                  <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4 control-label label-required">Kota:</label>
                        <div class="col-md-8">
                          <input type="text" value="{{ $r->kota }}" class="form-control" disabled>
                        </div>
                    </div>
                  </div>


                  <div class="col-md-6">
                    <div class="form-group">
    				            <label class="col-md-4 control-label label-required">Tingkat Pendidikan:</label>
    				            <div class="col-md-8">
    				            	<input type="text" value="{{ $r->tingkat_pen }}" class="form-control" disabled>
    				            </div>
    				        </div>

                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
    				            <label class="col-md-4 control-label label-required">Jurusan:</label>
    				            <div class="col-md-8">
    				            	<input type="text" value="{{ $r->jurusan }}" class="form-control" disabled>
    				            </div>
    				        </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
    				            <label class="col-md-4 control-label label-required">No Ijazah:</label>
    				            <div class="col-md-8">
    				            	<input type="text" value="{{ $r->no_ijazah }}" class="form-control" disabled>
    				            </div>
    				        </div>
                  </div>


                  <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4 control-label label-required">Tahun Keluar:</label>
                        <div class="col-md-8">
                          <input type="text" value="{{ $r->date_keluar }}" class="form-control" disabled>
                        </div>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-actions text-center">
                      <input type="submit" value="Kembali" class="btn btn-danger">
                  </div>
                  </div>


  					</form>
          </div>

@endforeach
					<!-- /separated form outside panel -->

@endsection
