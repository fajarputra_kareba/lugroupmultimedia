@extends('master.master')

@section('body')

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
  <ul class="breadcrumb">
    <li style="color:#fff"><a href="index.html">Home</a></li>
    <li class="active">Kepegawaian</li>
  </ul>
</div>
<!-- /breadcrumbs line -->
<!-- Alert -->
<!-- <div class="alert alert-warning fade in block">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
</div> -->
<!-- /alert -->
<!-- Simple contact form -->
@foreach ($riwayat as $r)
    <form action="{{ route('pendidikan.update', $r->rpen_id) }}" role="form" method="post">
          {{ csrf_field() }}
            {{ method_field('PATCH') }}
          <div class="panel panel-default">
          <div class="panel-heading"><h6 class="panel-title"><i class="icon-pencil3"></i> Form Pengisian Riwayat Pendidikan</h6></div>
          <div class="panel-body">
          <div class="form-group">
            <div class="row">
              <div class="col-md-6" style="padding:10px;">
                <label>Sekolah/Universitas:</label>
                    <input type="text" name="sekolah" value="{{ $r->sekolah }}" class="form-control">
                    <input type="hidden" name="id" value="{{ $r->user_id }}" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Kota:</label>
                    <input type="text" name="kota" value="{{ $r->kota }}" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Tingkat Pendidikan:</label>
                    <input type="text" name="tingkat" value="{{ $r->tingkat_pen }}" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Jurusan:</label>
                    <input type="text" name="jurusan" value="{{ $r->jurusan }}" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>No Ijazah:</label>
                    <input type="text" name="ijazah" value="{{ $r->no_ijazah }}" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Tanggal Lulus:</label>
                    <input type="text" name="lulus" value="{{ $r->date_keluar }}" class="form-control">
              </div>

            </div>
          </div>



          <div class="form-actions text-right">
            <input type="reset" value="Cancel" class="btn btn-danger">
            <input type="submit" value="Insert" class="btn btn-primary">
          </div>

        </div>
      </div>
    </form>
@endforeach
    <!-- /simple contact form -->
@endsection
