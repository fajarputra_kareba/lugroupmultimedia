@extends('master.master')

@section('body')

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
  <ul class="breadcrumb">
    <li style="color:#fff"><a href="index.html">Home</a></li>
    <li class="active">Kepegawaian</li>
  </ul>
</div>
<!-- /breadcrumbs line -->
<!-- Alert -->
<!-- <div class="alert alert-warning fade in block">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
</div> -->
<!-- /alert -->
<!-- Simple contact form -->
    <form action="{{ route('pegawai.update', $user) }}" role="form" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          {{ method_field('PATCH') }}
          <div class="panel panel-default">
          <div class="panel-heading"><h6 class="panel-title"><i class="icon-pencil3"></i> Form Pengisian Data </h6></div>
          <div class="panel-body">

          <div class="form-group">
            <div class="row">
              <div class="col-md-6" style="padding:10px;">
                <label>Nama Pegawai:</label>
                    <input type="text" name="name" value="{{ $user->name }}" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>Email:</label>
                    <input type="email" name="email" value="{{ $user->email }}" class="form-control">
              </div>

              <div class="col-md-6" style="padding:10px;">
                <label>NIK:</label>
                    <input type="text" name="nik" value="{{ $user->nik }}" class="form-control">
              </div>
              <div class="col-md-6" style="padding:10px;">
                <label>DEPARTMENT:</label>
                    <input type="text" name="department" value="{{ $user->department }}" class="form-control">
              </div>
              <div class="col-md-6" style="padding:10px;">
                <label>JABATAN:</label>
                    <input type="text" name="jabatan" value="{{ $user->jabatan }}" class="form-control">
              </div>
              <div class="col-md-6" style="padding:10px;">
                <label>TANGGAL MULAI KERJA:</label>
                    <input type="date" name="tanggal" class="form-control" value="{{ $user->tanggal_kerja }}">
              </div>
              <div class="col-md-6" style="padding:10px;">
                  <label>STATUS PEGAWAI:</label>
                    <select name="gender" data-placeholder="Choose an option..." class="form-control" tabindex="2">
                      <option value="Laki-Laki">Laki-Laki</option>
                      <option value="Perempuan">Perempuan</option>
                    </select>
                </div>
              <div class="col-md-6" style="padding:10px;">
                  <label>STATUS PEGAWAI:</label>
                    <select name="status" data-placeholder="Choose an option..." class="form-control" tabindex="2">
                      <option value="Tetap">Tetap</option>
                      <option value="Kontrak">Kontrak</option>
                    </select>
                </div>

              <div class="col-md-6" style="padding:10px;">
                <label>photo:</label>
                    <input type="file" name="gambar" class="form-control" value="{{ $user->photo }}">
              </div>

            </div>
          </div>



          <div class="form-actions text-right">
            <a href="{{ route('pegawai.index') }}" class="btn btn-danger">Cencel</a>
            <input type="submit" value="Insert" class="btn btn-primary">
          </div>

        </div>
      </div>
    </form>
    <!-- /simple contact form -->
@endsection
