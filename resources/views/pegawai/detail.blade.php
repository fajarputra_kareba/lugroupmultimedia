@extends('master.master')

@section('body')
  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Dashboard</li>
    </ul>
  </div>
  <!-- /breadcrumbs line -->
  <!-- Alert -->
  <!-- <div class="alert alert-warning fade in block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
  </div> -->
  <!-- /alert -->
  <!-- Inside tabs -->
			            <div class="panel panel-default">
			                <div class="panel-heading"><h6 class="panel-title"><i class="icon-profile"></i> Detail Data Pegawai</h6></div>
			                <div class="tabbable">
			                    <ul class="nav nav-tabs toolbar-tabs">
			                        <li class="active"><a href="#table-tab1" data-toggle="tab"><i class="icon-user4"></i>Data Utama</a></li>
                              <li><a href="#table-tab2" data-toggle="tab"><i class="icon-address-book"></i></i>Posisi & Jabatan</a></li>
                              <li><a href="#table-tab3" data-toggle="tab"><i class="icon-book"></i>Data Pendidikan</a></li>
                              <li><a href="#table-tab4" data-toggle="tab"><i class="icon-clipboard"></i>Riwayat Jabatan</a></li>
                              <li><a href="#table-tab5" data-toggle="tab"><i class="icon-certificate"></i>Kursus/Diklat</a></li>
                              <li><a href="#table-tab6" data-toggle="tab"><i class="icon-home6"></i>Data Keluarga</a></li>
                              <li><a href="#table-tab7" data-toggle="tab"><i class="icon-medal"></i>Penghargaan</a></li>
                           </ul>
			                    <div class="tab-content table-tabs">
			                     <div class="tab-pane fade active in" id="table-tab1">
                            <!-- Separated form outside panel -->
                              @foreach ($users as $u)
                                        <div class="panel-body">
                                          <form class="form-horizontal form-separate" action="{{ route( 'pegawai.detail', $u->id )}}" role="form">
                                						<h6><i class="icon-user4"></i> Detail Utama</h6>

                                            <div class="col-md-12">
                                              <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label label-required">NIK:</label>
                                                    <div class="col-md-8">
                                                      <input type="text" value="{{ $u->nik }}" class="form-control" disabled>
                                                    </div>
                                                </div>
                                              </div>

                                            </div>

                                            <div class="col-md-12">
                                              <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label label-required">Nama Depan:</label>
                                                    <div class="col-md-8">
                                                      <input type="text" value="{{ $u->name }}" class="form-control" disabled>
                                                    </div>
                                                </div>

                                              </div>

                                              <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label label-required">Nama Belakang:</label>
                                                    <div class="col-md-8">
                                                      <input type="text" value="" class="form-control" disabled>
                                                    </div>
                                                </div>

                                              </div>
                                            </div>

                                            <div class="col-md-12">
                                              <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label label-required">Gelar Depan:</label>
                                                    <div class="col-md-8">
                                                      <input type="text" value="" class="form-control" disabled>
                                                    </div>
                                                </div>
                                              </div>


                                              <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label label-required">Gelar Belakang:</label>
                                                    <div class="col-md-8">
                                                      <input type="text" value="" class="form-control" disabled>
                                                    </div>
                                                </div>
                                              </div>
                                            </div>

                                            <div class="col-md-12">
                                              <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label label-required">Provinsi Tempat Lahir:</label>
                                                    <div class="col-md-8">
                                                      <input type="text" value="" class="form-control" disabled>
                                                    </div>
                                                </div>
                                              </div>

                                              <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label label-required">Kab/Kota Tempat Lahir:</label>
                                                    <div class="col-md-8">
                                                      <input type="text" value="" class="form-control" disabled>
                                                    </div>
                                                </div>
                                              </div>
                                            </div>

                                            <div class="col-md-12">
                                              <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label label-required">Tanggal Lahir:</label>
                                                    <div class="col-md-8">
                                                      <input type="text" value="" class="form-control" disabled>
                                                    </div>
                                                </div>
                                              </div>

                                              <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label label-required">Agama:</label>
                                                    <div class="col-md-8">
                                                      <input type="text" value="" class="form-control" disabled>
                                                    </div>
                                                </div>
                                              </div>
                                            </div>

                                            <div class="col-md-12">
                                              <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label label-required">Jenis Kelamin:</label>
                                                    <div class="col-md-8">
                                                      <input type="text" value="{{ $u->gender }}" class="form-control" disabled>
                                                    </div>
                                                </div>
                                              </div>

                                              <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label label-required">Status Perkawinan:</label>
                                                    <div class="col-md-8">
                                                      <input type="text" value="" class="form-control" disabled>
                                                    </div>
                                                </div>
                                              </div>
                                            </div>

                                            <div class="col-md-12">
                                              <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label label-required">Email:</label>
                                                    <div class="col-md-8">
                                                      <input type="text" value="{{ $u->email }}" class="form-control" disabled>
                                                    </div>
                                                </div>
                                              </div>

                                              <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label label-required">Alamat:</label>
                                                    <div class="col-md-8">
                                                      <input type="text" value="" class="form-control" disabled>
                                                    </div>
                                                </div>
                                              </div>
                                            </div>

                                            <div class="col-md-12">
                                              <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label label-required">Provinsi:</label>
                                                    <div class="col-md-8">
                                                      <input type="text" value="" class="form-control" disabled>
                                                    </div>
                                                </div>
                                              </div>

                                              <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label label-required">Kota:</label>
                                                    <div class="col-md-8">
                                                      <input type="text" value="" class="form-control" disabled>
                                                    </div>
                                                </div>
                                              </div>
                                            </div>

                                            <div class="col-md-12">
                                              <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label label-required">Kode POS:</label>
                                                    <div class="col-md-8">
                                                      <input type="text" value="" class="form-control" disabled>
                                                    </div>
                                                </div>
                                              </div>

                                              <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label label-required">No. Telephon:</label>
                                                    <div class="col-md-8">
                                                      <input type="text" value="" class="form-control" disabled>
                                                    </div>
                                                </div>
                                              </div>
                                            </div>

                                            <div class="col-md-12">
                                              <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label label-required">No. HP:</label>
                                                    <div class="col-md-8">
                                                      <input type="text" value="" class="form-control" disabled>
                                                    </div>
                                                </div>
                                              </div>

                                            </div>

                                            <div class="col-md-12">
                                              <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label label-required">Tandatangan:</label>
                                                    <div class="col-md-8">
                                                      <input type="text" value="Tidak Ada" class="form-control" disabled>
                                                    </div>
                                                </div>
                                              </div>

                                              <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label label-required">Foto Pegawai:</label>
                                                    <div class="col-md-8">
                                                      <img src="{{ asset('image/'.$u->photo)  }}" style="height:150px; width:150px;">

                                                    </div>
                                                </div>
                                              </div>


                                            </div>

                                                <div class="col-md-12">
                                                  <div class="form-actions text-center">
                                                    <input type="submit" value="Kembali" class="btn btn-danger">
                                                </div>
                                                </div>


                                					</form>
                                        </div>
                              @endforeach
                          </div>

                          <div class="tab-pane fade" id="table-tab2">

                            <div class="panel-body">
                              @foreach($posisi as $key => $pos)
                              <form class="form-horizontal form-separate" action="{{ route( 'pegawai.detail', $u->id )}}" role="form">
                                <h6><i class="icon-user4"></i> Posisi Dan Jabatan</h6>

                                <div class="col-md-12">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label label-required">Department:</label>
                                        <div class="col-md-8">
                                          <input type="text" value="{{ $pos->pos_department }}" class="form-control" disabled>
                                        </div>
                                    </div>
                                  </div>

                                  <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label label-required">Nama Jabatan:</label>
                                        <div class="col-md-8">
                                          <input type="text" value="{{ $pos->pos_jabatan }}" class="form-control" disabled>
                                        </div>
                                    </div>

                                  </div>

                                </div>

                                <div class="col-md-12">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label label-required">Tipe Pegawai:</label>
                                        <div class="col-md-8">
                                          <input type="text" value="{{ $pos->pos_tipe }}" class="form-control" disabled>
                                        </div>
                                    </div>

                                  </div>

                                  <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label label-required">Tanggal Mulai Kerja:</label>
                                        <div class="col-md-8">
                                          <input type="text" value="{{ $pos->pos_mulaikerja }}" class="form-control" disabled>
                                        </div>
                                    </div>

                                  </div>
                                </div>

                                <div class="col-md-12">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label label-required">Cabang Perusahaan:</label>
                                        <div class="col-md-8">
                                          <input type="text" value="{{ $pos->pos_cabang }}" class="form-control" disabled>
                                        </div>
                                    </div>
                                  </div>


                                </div>

                                    <div class="col-md-12">
                                      <div class="form-actions text-center">
                                        <input type="submit" value="Kembali" class="btn btn-danger">
                                    </div>
                                    </div>


                              </form>
                              @endforeach
                            </div>
                          </div>

			                    <div class="tab-pane fade" id="table-tab3">
							            <div class="panel-body">
                            <div class="pull-right">
                            <a href="{{ route('pendidikan.create') }}" class="btn btn-xs btn-success" style="margin-bottom:10px;"><i class="icon-file-plus"></i>Tambah Pendidikan</a>
                            <button style="margin-bottom: 10px" class="btn btn-xs btn-danger delete_all" data-url="{{ url('pendidikanDeleteAll') }}"><i class="icon-file-remove"></i>Delete Pendidikan</button>
                            </div>
                            <div class="table-responsive">
                              <table class="table table-bordered table-check">
                                <thead>
                                  <tr>
                                    <th><input type="checkbox"  id="master"></th>
                                    <th>#</th>
                                    <th>Sekolah/Universitas</th>
                                    <th>Kota</th>
                                    <th>Tingkat Pendidikan</th>
                                    <th>Jurusan</th>
                                    <th>No. Ijazah</th>
                                    <th>Tahun Selesai</th>
                                    <th>Aksi</th>
                                  </tr>
                                </thead>
                                <tbody>
                                @foreach($riwayat as $key => $r)
                                     <tr id="tr_{{$r->rpen_id}}">
                                    <td><input type="checkbox" name="checkRow" class="sub_chk" data-id="{{$r->rpen_id}}"/></td>
                                    <td>{{ ++$key }}</td>
                                    <td>{{ $r->sekolah }}</td>
                                    <td>{{ $r->kota }}</td>
                                    <td>{{ $r->tingkat_pen }}</td>
                                    <td>{{ $r->jurusan }}</td>
                                    <td>{{ $r->no_ijazah }}</td>
                                    <td>{{ $r->date_keluar }}</td>
                                    <td class="text-center">
                                      <form class="" action="{{ route('pendidikan.destroy', $r->rpen_id) }}" method="post">
                                          {{ csrf_field() }}
                                          {{ method_field('Delete') }}
                                          <input type="hidden" name="userid" value="{{ $r->user_id }}">
                                          <button type="submit" name="button" class="btn btn-default btn-xs btn-icon tip" data-original-title="Remove"><i class="icon-remove2"></i></button>
                                          <a href="{{ route('pendidikan.detail', $r->rpen_id) }}" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Detail"><i class="icon-eye7"></i></a>
                                          <a href="{{ route('pendidikan.edit', $r->rpen_id) }}" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Update"><i class="icon-pencil"></i></a>
                                      </form>
                                    </td>
                                  </tr>
                                @endforeach
                                  </tbody>
                              </table>
							                </div>
							            </div>
                          </div>
                          <div class="tab-pane fade" id="table-tab4">
                         <div class="panel-body">
                           <div class="pull-right">
                           <a href="{{ route('pengalaman.create') }}" class="btn btn-xs btn-success" style="margin-bottom:10px;"><i class="icon-file-plus"></i>Tambah Riwayat Jabatan</a>
                           <button style="margin-bottom: 10px" class="btn btn-xs btn-danger delete_all" data-url="{{ url('pengalamanDeleteAll') }}"><i class="icon-file-remove"></i>Delete Riwayat Jabatan</button>
                           </div>
                           <div class="table-responsive">
                             <table class="table table-bordered table-check">
                               <thead>
                                 <tr>
                                   <th><input type="checkbox"  id="master"></th>
                                   <th>#</th>
                                   <th>Nama Perusahaan</th>
                                   <th>Jabatan</th>
                                   <th>Unit Kerja</th>
                                   <th>nomor Sk</th>
                                   <th>Tanggal SK</th>
                                   <th>Aksi</th>
                                 </tr>
                               </thead>
                               <tbody>
                              @foreach($pengalaman as $key => $p)
                                <tr id="tr_{{$p->pengalaman_id}}">
                                   <td><input type="checkbox" name="checkRow" class="sub_chk" data-id="{{$p->pengalaman_id}}"/></td>
                                   <td>{{ ++$key }}</td>
                                   <td>{{ $p->nama_perusahaan }}</td>
                                   <td>{{ $p->jabatan }}</td>
                                   <td>{{ $p->unit_kerja }}</td>
                                   <td>{{ $p->no_sk }}</td>
                                   <td>{{ $p->tgl_sk }}</td>
                                   <td class="text-center">
                                     <form class="" action="{{ route('pengalaman.destroy', $p->pengalaman_id) }}" method="post">
                                         {{ csrf_field() }}
                                         {{ method_field('Delete') }}
                                         <input type="hidden" name="userid" value="{{ $p->user_id }}">
                                         <button type="submit" name="button" class="btn btn-default btn-xs btn-icon tip" data-original-title="Remove"><i class="icon-remove2"></i></button>
                                         <a href="{{ route('pengalaman.detail', $p->pengalaman_id) }}" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Detail"><i class="icon-eye7"></i></a>
                                         <a href="{{ route('pengalaman.edit', $p->pengalaman_id) }}" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Update"><i class="icon-pencil"></i></a>
                                     </form>
                                   </td>
                                 </tr>
                               @endforeach
                                </tbody>
                             </table>
                             </div>
                         </div>
                         </div>
                         <div class="tab-pane fade" id="table-tab5">
                         <div class="panel-body">
                           <div class="pull-right">
                             <a href="{{ route('pelatihan.create') }}" class="btn btn-xs btn-success" style="margin-bottom:10px;"><i class="icon-file-plus"></i>Tambah Diklat</a>
                             <button style="margin-bottom: 10px" class="btn btn-xs btn-danger delete_all" data-url="{{ url('pelatihanDeleteAll') }}"><i class="icon-file-remove"></i>Delete Diklat</button>
                           </div>
                           <div class="table-responsive">
                             <table class="table table-bordered table-check">
                               <thead>
                                 <tr>
                                   <th><input type="checkbox"  id="master"></th>
                                   <th>#</th>
                                   <th>Title</th>
                                   <th>Lokasi</th>
                                   <th>Tanggal Mulai</th>
                                   <th>Tanggal Selesai</th>
                                   <th>Penyelenggara</th>
                                   <th>Nomor Sertifikat</th>
                                   <th>Tanggal Sertifikat</th>
                                   <th>Aksi</th>
                                 </tr>
                               </thead>
                               <tbody>
                              @foreach($pelatihan as $key => $pel)
                                    <tr id="tr_{{$pel->pelatihan_id}}">
                                   <td><input type="checkbox" name="checkRow" class="sub_chk" data-id="{{$pel->pelatihan_id}}"/></td>
                                   <td>{{ ++$key }}</td>
                                   <td>{{ $pel->title }}</td>
                                   <td>{{ $pel->lokasi }}</td>
                                   <td>{{ $pel->date_mulai }}</td>
                                   <td>{{ $pel->date_selesai }}</td>
                                   <td>{{ $pel->penyelenggara }}</td>
                                   <td>{{ $pel->no_sertifikat }}</td>
                                   <td>{{ $pel->tgl_sertifikat }}</td>
                                   <td class="text-center">
                                     <form class="" action="{{ route('pelatihan.destroy', $pel->pelatihan_id) }}" method="post">
                                         {{ csrf_field() }}
                                         {{ method_field('Delete') }}
                                         <input type="hidden" name="userid" value="{{ $pel->user_id }}">
                                         <button type="submit" name="button" class="btn btn-default btn-xs btn-icon tip" data-original-title="Remove"><i class="icon-remove2"></i></button>
                                         <a href="{{ route('pelatihan.detail', $pel->pelatihan_id) }}" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Detail"><i class="icon-eye7"></i></a>
                                         <a href="{{ route('pelatihan.edit', $pel->pelatihan_id) }}" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Update"><i class="icon-pencil"></i></a>
                                     </form>
                                   </td>
                                 </tr>
                               @endforeach
                                 </tbody>
                             </table>
                             </div>
                         </div>
                        </div>

                        <div class="tab-pane fade" id="table-tab6">
                         <div class="panel-body">
                           <div class="pull-right">
                           <a href="{{ route('family.family') }}" class="btn btn-xs btn-success" style="margin-bottom:10px;"><i class="icon-file-plus"></i>Tambah Keluarga</a>
                           <button style="margin-bottom: 10px" class="btn btn-xs btn-danger delete_all" data-url="{{ url('familyDeleteAll') }}"><i class="icon-file-remove"></i>Delete Keluarga</button>
                           </div>
                          <div class="table-responsive">
                            <table class="table table-bordered table-check">
                             <thead>
                               <tr>
                                 <th><input type="checkbox"  id="master"></th>
                                  <th>#</th>
                                  <th>Nama</th>
                                  <th>Tempat Lahir</th>
                                  <th>Tanggal Lahir</th>
                                  <th>Jenis Kelamin</th>
                                  <th>Pendidikan</th>
                                  <th>Pekerjaan</th>
                                  <th>Agama</th>
                                  <th>Status Pernikahan</th>
                                  <th>Status Hidup</th>
                                  <th>Aksi</th>
                               </tr>
                             </thead>
                             <tbody>
                             @foreach($family as $key => $fam)
                                   <tr id="tr_{{$fam->fam_id}}">
                                 <td>
                                   <input type="checkbox" name="checkRow" class="sub_chk" data-id="{{$fam->fam_id}}"/>
                                 </td>
                                  <td>{{ ++$key }}</td>
                                  <td>{{ $fam->fam_nama }}</td>
                                  <td>{{ $fam->fam_tempat_lahir }}</td>
                                  <td>{{ $fam->fam_tgl_lahir }}</td>
                                  <td>{{ $fam->fam_gender }}</td>
                                  <td>{{ $fam->fam_pendidikan }}</td>
                                  <td>{{ $fam->fam_pekerjaan }}</td>
                                  <td>{{ $fam->fam_agama }}</td>
                                  <td>{{ $fam->fam_status_nikah }}</td>
                                  <td>{{ $fam->fam_status_hidup }}</td>
                                  </td>
                                  <td class="text-center">
                                    <form class="" action="{{ route('family.destroy', $fam->fam_id) }}" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('Delete') }}
                                        <input type="hidden" name="userid" value="{{ $fam->fam_user_id }}">
                                        <button type="submit" name="button" class="btn btn-default btn-xs btn-icon tip" data-original-title="Remove"><i class="icon-remove2"></i></button>
                                        <a href="{{ route('family.detail', $fam->fam_id) }}" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Detail"><i class="icon-eye7"></i></a>
                                        <a href="{{ route('family.edit', $fam->fam_id) }}" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Update"><i class="icon-pencil"></i></a>
                                    </form>
                                  </td>
                               </tr>
                              @endforeach
                            </tbody>
                           </table>
                         </div>
                       </div>
                       </div>

                       <div class="tab-pane fade" id="table-tab7">
                        <div class="panel-body">
                          <div class="pull-right">
                          <a href="{{ route('reward.reward') }}" class="btn btn-xs btn-success" style="margin-bottom:10px;"><i class="icon-file-plus"></i>Tambah Penghargaan</a>
                          <button style="margin-bottom: 10px" class="btn btn-xs btn-danger delete_all" data-url="{{ url('rewardDeleteAll') }}"><i class="icon-file-remove"></i>Delete Penghargaan</button>
                          </div>
                         <div class="table-responsive">
                           <table class="table table-bordered table-check">
                            <thead>
                              <tr>
                                <th><input type="checkbox"  id="master"></th>
                                 <th>#</th>
                                 <th>Nama Penghargaan</th>
                                 <th>Nama Pemberi Penghargaan</th>
                                 <th>Nomor SK</th>
                                 <th>Tanggal SK</th>
                                 <th>Aksi</th>
                              </tr>
                            </thead>
                            <tbody>
                            @foreach($reward as $key => $re)
                                  <tr id="tr_{{$re->rew_id}}">
                                <td>
                                  <input type="checkbox" name="checkRow" class="sub_chk" data-id="{{$re->rew_id}}"/>
                                </td>
                                 <td>{{ ++$key }}</td>
                                 <td>{{ $re->rew_title }}</td>
                                 <td>{{ $re->rew_nama_pemberi }}</td>
                                 <td>{{ $re->rew_nosk }}</td>
                                 <td>{{ $re->rew_tgl_keputusan }}</td>
                                 </td>
                                 <td class="text-center">
                                   <form class="" action="{{ route('reward.destroy', $re->rew_id) }}" method="post">
                                       {{ csrf_field() }}
                                       {{ method_field('Delete') }}
                                       <input type="hidden" name="userid" value="{{ $re->rew_user_id }}">
                                       <button type="submit" name="button" class="btn btn-default btn-xs btn-icon tip" data-original-title="Remove"><i class="icon-remove2"></i></button>
                                       <a href="{{ route('reward.detail', $re->rew_id) }}" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Detail"><i class="icon-eye7"></i></a>
                                       <a href="{{ route('reward.edit', $re->rew_id) }}" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Update"><i class="icon-pencil"></i></a>
                                   </form>
                                 </td>
                              </tr>
                             @endforeach
                           </tbody>
                          </table>
                        </div>
                      </div>
                      </div>

			                  </div>
			                </div>
			            </div>
			            <!-- /inside tabs -->
                  <script type="text/javascript">
                      $(document).ready(function () {


                          $('#master').on('click', function(e) {
                           if($(this).is(':checked',true))
                           {
                              $(".sub_chk").prop('checked', true);
                           } else {
                              $(".sub_chk").prop('checked',false);
                           }
                          });


                          $('.delete_all').on('click', function(e) {


                              var allVals = [];
                              $(".sub_chk:checked").each(function() {
                                  allVals.push($(this).attr('data-id'));
                              });


                              if(allVals.length <=0)
                              {
                                  alert("Please select row.");
                              }  else {


                                  var check = confirm("Are you sure you want to delete this row?");
                                  if(check == true){


                                      var join_selected_values = allVals.join(",");


                                      $.ajax({
                                          url: $(this).data('url'),
                                          type: 'DELETE',
                                          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                          data: 'ids='+join_selected_values,
                                          success: function (data) {
                                              if (data['success']) {
                                                  $(".sub_chk:checked").each(function() {
                                                      $(this).parents("tr").remove();
                                                  });
                                                  alert(data['success']);
                                              } else if (data['error']) {
                                                  alert(data['error']);
                                              } else {
                                                  alert('Whoops Something went wrong!!');
                                              }
                                          },
                                          error: function (data) {
                                              alert(data.responseText);
                                          }
                                      });


                                    $.each(allVals, function( index, value ) {
                                        $('table tr').filter("[data-row-id='" + value + "']").remove();
                                    });
                                  }
                              }
                          });


                          $('[data-toggle=confirmation]').confirmation({
                              rootSelector: '[data-toggle=confirmation]',
                              onConfirm: function (event, element) {
                                  element.trigger('confirm');
                              }
                          });


                          $(document).on('confirm', function (e) {
                              var ele = e.target;
                              e.preventDefault();


                              $.ajax({
                                  url: ele.href,
                                  type: 'DELETE',
                                  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                  success: function (data) {
                                      if (data['success']) {
                                          $("#" + data['tr']).slideUp("slow");
                                          alert(data['success']);
                                      } else if (data['error']) {
                                          alert(data['error']);
                                      } else {
                                          alert('Whoops Something went wrong!!');
                                      }
                                  },
                                  error: function (data) {
                                      alert(data.responseText);
                                  }
                              });


                              return false;
                          });
                      });
                  </script>
@endsection
