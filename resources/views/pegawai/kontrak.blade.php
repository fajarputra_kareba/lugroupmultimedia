@extends('master.master')


@section('body')
  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Dashboard</li>
    </ul>
  </div>
  <script type="text/javascript">
      $(function() {
        $("#q").autocomplete({
          source: "{{ URL('pegawai/autocompletekontrak') }}",
          minlength: 2,
          select: function(event, ui) {
            $("q").html(ui.item.value);
            onchange= this.form.submit()
          }
        });
      });
  </script>

  <!-- /breadcrumbs line -->
  <!-- Alert -->
  <!-- <div class="alert alert-warning fade in block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
  </div> -->
  <!-- /alert -->
  <!-- Table with checkboxes -->
  <form action="{{ route('pegawai.searchkontrak') }}" role="form" method="post">
        {{ csrf_field() }}
        <div class="panel panel-default">
        <div class="panel-heading"><h6 class="panel-title"><i class="icon-pencil3"></i> Filter Data Pegawai</h6></div>
        <div class="panel-body">
        <div class="form-group">
          <div class="row">
          <div class="col-md-6" style="padding:10px;">
              <label>Nama:</label>
                  <input type="text" name="name" class="form-control" id="q">
          </div>

            <div class="col-md-6" style="padding:10px;">
              <label>Gender:</label>
              <select name="gender" data-placeholder="Pilih Gender..." class="select-full">
                <option value="">Pilih Gender</option>
                <option value="Laki-Laki">Laki-Laki</option>
                <option value="Perempuan">Perempuan</option>
              </select>
            </div>

          </div>
        </div>



        <div class="form-actions text-center">
          <input type="reset" value="Reset" class="btn btn-danger">
          <input type="submit" value="Cari" class="btn btn-success">
        </div>

      </div>
    </div>
  </form>


               <div class="panel panel-default">
  			        <div class="panel-heading">
                  <div class="pull-right">
                    <a href="{{ route('pegawai.create') }}" class="btn btn-xs btn-success" style="margin-bottom:10px; margin-top:10px;"><i class="icon-file-plus"></i>Tambah Data</a>
                    <button style="margin-bottom: 10px; margin-top:10px;" class="btn btn-xs btn-danger delete_all" data-url="{{ url('pegawaiDeleteAll') }}"><i class="icon-file-remove"></i>Hapus Data</button>
                  </div>

                  <h6 class="panel-title"><i class="icon-checkbox-partial"></i> Data Pegawai</h6>
                </div>
                 <div class="table-responsive">
                  <table class="table table-bordered table-check">
  									<thead>
  										<tr>
  											<th><input type="checkbox"  id="master"></th>
                        <th>#</th>
                        <th>Photo</th>
                        <th>Nama</th>
                        <th>TANGGAL KERJA</th>
                        <th>JENIS KELAMIN</th>
                        <th>STATUS PEGAWAI</th>
                        <th>Aksi</th>
  										</tr>
  									</thead>
  									<tbody>
                    @if($users->count())
                    <?php $no = $users->firstItem() - 1 ; ?>
                    @foreach($users as $key => $user)
                      <?php $no++ ;?>
                      <tr id="tr_{{$user->id}}">
  											<td><input type="checkbox" name="checkRow" class="sub_chk" data-id="{{$user->id}}"/></td>
                        <td>{{ $no }}</td>
                        <td width="70px"><img src="{{ asset('image/'.$user->photo)  }}" style="height:50px; width:50px;"></td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->tanggal_kerja }}</td>
                        <td>{{ $user->gender }}</td>
                        <td>{{ $user->status }}</td>
                        <td class="text-center">
                          <form class="" action="{{ route('pegawai.destroy', $user->id) }}" method="post">
                              {{ csrf_field() }}
                              {{ method_field('Delete') }}
                              <button type="submit" name="button" class="btn btn-default btn-xs btn-icon tip" data-original-title="Remove"><i class="icon-remove2"></i></button>
                              <a href="{{ route('pegawai.detail', $user->id) }}" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Detail"><i class="icon-eye7"></i></a>
                              <a href="{{ route('pegawai.edit', $user) }}" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Update"><i class="icon-pencil"></i></a>
                          </form>
                        </td>
  										</tr>
                    @endforeach
                      @endif
  									</tbody>
  								</table>
                    <?php echo str_replace('/?', '?', $users->render()); ?>
  							</div>
  				        </div>
  				        <!-- /table with checkboxes -->

                  <script type="text/javascript">
                      $(document).ready(function () {


                          $('#master').on('click', function(e) {
                           if($(this).is(':checked',true))
                           {
                              $(".sub_chk").prop('checked', true);
                           } else {
                              $(".sub_chk").prop('checked',false);
                           }
                          });


                          $('.delete_all').on('click', function(e) {


                              var allVals = [];
                              $(".sub_chk:checked").each(function() {
                                  allVals.push($(this).attr('data-id'));
                              });


                              if(allVals.length <=0)
                              {
                                  alert("Please select row.");
                              }  else {


                                  var check = confirm("Are you sure you want to delete this row?");
                                  if(check == true){


                                      var join_selected_values = allVals.join(",");


                                      $.ajax({
                                          url: $(this).data('url'),
                                          type: 'DELETE',
                                          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                          data: 'ids='+join_selected_values,
                                          success: function (data) {
                                              if (data['success']) {
                                                  $(".sub_chk:checked").each(function() {
                                                      $(this).parents("tr").remove();
                                                  });
                                                  alert(data['success']);
                                              } else if (data['error']) {
                                                  alert(data['error']);
                                              } else {
                                                  alert('Whoops Something went wrong!!');
                                              }
                                          },
                                          error: function (data) {
                                              alert(data.responseText);
                                          }
                                      });


                                    $.each(allVals, function( index, value ) {
                                        $('table tr').filter("[data-row-id='" + value + "']").remove();
                                    });
                                  }
                              }
                          });


                          $('[data-toggle=confirmation]').confirmation({
                              rootSelector: '[data-toggle=confirmation]',
                              onConfirm: function (event, element) {
                                  element.trigger('confirm');
                              }
                          });


                          $(document).on('confirm', function (e) {
                              var ele = e.target;
                              e.preventDefault();


                              $.ajax({
                                  url: ele.href,
                                  type: 'DELETE',
                                  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                  success: function (data) {
                                      if (data['success']) {
                                          $("#" + data['tr']).slideUp("slow");
                                          alert(data['success']);
                                      } else if (data['error']) {
                                          alert(data['error']);
                                      } else {
                                          alert('Whoops Something went wrong!!');
                                      }
                                  },
                                  error: function (data) {
                                      alert(data.responseText);
                                  }
                              });


                              return false;
                          });
                      });
                  </script>

@endsection
