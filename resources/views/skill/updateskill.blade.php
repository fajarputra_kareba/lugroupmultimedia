@extends('master.master')

@section('body')

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
  <ul class="breadcrumb">
    <li style="color:#fff"><a href="index.html">Home</a></li>
    <li class="active">Kepegawaian</li>
  </ul>
</div>
<!-- /breadcrumbs line -->
<!-- Alert -->
<!-- <div class="alert alert-warning fade in block">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
</div> -->
<!-- /alert -->
<!-- Simple contact form -->
@foreach ($skills as $skill)
    <form action="{{ route('skill.update', $skill->skill_id) }}" role="form" method="post">
          {{ csrf_field() }}
          {{ method_field('PATCH') }}
          <div class="panel panel-default">
          <div class="panel-heading"><h6 class="panel-title"><i class="icon-pencil3"></i> Form Pengisian Data </h6></div>
          <div class="panel-body">

            <div class="form-group">
              <div class="row">
                <div class="col-md-6" style="padding:10px;">
                  <label>Skill:</label>
                      <input type="text" name="skill" value="{{ $skill->skill }}" class="form-control">
                </div>

                <div class="col-md-6" style="padding:10px;">
                  <label>Deskripsi Skill:</label>
                      <input type="text" name="deskripsi" value="{{ $skill->deskripsi }}" class="form-control">
                </div>
              </div>
            </div>

          <div class="form-actions text-right">
            <a href="{{ route('pegawai.index') }}" class="btn btn-danger">Cencel</a>
            <input type="submit" value="Insert" class="btn btn-primary">
          </div>

        </div>
      </div>
    </form>
  @endforeach
    <!-- /simple contact form -->
@endsection
