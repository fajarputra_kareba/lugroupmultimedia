@extends('master.master')

@section('body')

	<!-- Breadcrumbs line -->
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="index.html">Home</a></li>
			<li class="active">Berita</li>
		</ul>

		<div class="visible-xs breadcrumb-toggle">
			<a class="btn btn-link btn-lg btn-icon" data-toggle="collapse" data-target=".breadcrumb-buttons"><i class="icon-menu2"></i></a>
		</div>
	</div>
	<!-- /breadcrumbs line -->

  <!-- Pre-scrollable table -->
			            <div class="panel panel-default">
			                <div class="panel-heading">
												<div class="pull-right">
													<a href="{{ route('berita.create') }}" class="btn btn-xs btn-success" style="margin-bottom:10px; margin-top:10px;"><i class="icon-file-plus"></i>Tambah Data</a>
												</div>

												<h6 class="panel-title"><i class="icon-newspaper"></i> Berita Perusahaan</h6>
											</div>
			            	<div class="table-responsive">
				                <table class="table table-striped">
				                    <thead>
				                        <tr>
				                            <th>#</th>
				                            <th>Title</th>
				                            <th>Berita</th>
				                            <th>Penulis</th>
																		<th>Terbit</th>
																		<th>Aksi</th>
				                        </tr>
				                    </thead>
				                    <tbody>
														@if($berita->count())
															@php
																$no = $berita->firstItem() - 1;
															@endphp
															@foreach ($berita as $ber)
																@php
																	$no++;
																	$jumlahkarakter = 50;
																	$cetak = substr($ber->be_draft,$jumlahkarakter,1);
																	if($cetak !=" "){
																	while($cetak !=" "){
																		$i=1;
																		$jumlahkarakter=$jumlahkarakter+$i;
																		$cetak = substr($ber->be_draft,$jumlahkarakter,1);
																		}
																	}
																	$cetak = substr($ber->be_draft,0,$jumlahkarakter);
																@endphp
																<tr>
				                            <td>{{$no}}</td>
				                            <td>{{$ber->be_title}}</td>
				                            <td>{!! ($cetak) !!} <a data-toggle="modal" role="button" href="#{{$ber->be_id}}">Lihat Selengkapnya</a>

																			<!-- Default modal -->
																		<div id="{{$ber->be_id}}" class="modal fade" tabindex="-1" role="dialog">
																			<div class="modal-dialog">
																				<div class="modal-content">
																					<div class="modal-header">
																						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
																						<h4 class="modal-title">{{$ber->be_title}}</h4>
																					</div>

																					<div class="modal-body with-padding">
																						<h5 class="text-error">{{$ber->be_title}}</h5>
																						<p>{!!($ber->be_draft)!!}.</p>

																					</div>

																					<div class="modal-footer">
																						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																					</div>
																				</div>
																			</div>
																		</div>
																		<!-- /default modal -->
																		</td>
				                            <td>{{$ber->name}}</td>
																		<td>{{$ber->be_time_update}}</td>
																		<td class="text-center">
																			<form class="" action="{{ route('berita.destroy', $ber->be_id) }}" method="post">
																					{{ csrf_field() }}
																					{{ method_field('Delete') }}
																					<button type="submit" name="button" class="btn btn-default btn-xs btn-icon tip" data-original-title="Remove"><i class="icon-remove2"></i></button>
																					<a href="{{ route('berita.detail', $ber->be_id) }}" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Detail"><i class="icon-eye7"></i></a>
																					<a href="{{ route('berita.edit', $ber->be_id) }}" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Update"><i class="icon-pencil"></i></a>
																			</form>
																		</td>
				                        </tr>
															@endforeach
														@endif
				                    </tbody>

				                </table>
				            </div>
										<div class="well clearfix">
													<ul class="pagination pagination-right pull-right">
															<?php echo str_replace('/?', '?', $berita->render()); ?>
													</ul>
											</div>
				        </div>
								<!-- /pre-scrollable table -->

@endsection
