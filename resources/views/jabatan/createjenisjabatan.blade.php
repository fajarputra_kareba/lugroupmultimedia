@extends('master.master')


@section('body')
  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Dashboard</li>
    </ul>
  </div>

  <form action="{{url('jenisjabatan/insertjenisjabatan')}}" class="form-horizontal form-separate" method="post" >
    {{ csrf_field() }}
    <div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="block-inner text-danger">
                    <h6 class="heading-hr">Tambah Jabatan                        <small class="display-block">Mohon Isikan Dengan Sesuai dan Benar</small>
                    </h6>
                </div>
                <div class="table-responsive">
                    <div class="panel-heading" style="background:#2179cc">
                            <h6 class="panel-title" style=" color:#fff"><i class="icon-menu2"></i>Data Jabatan</h6>
                        </div><table width="100%" class="table">

                        <tbody><tr>
                            <td>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="OfficeName" class="col-sm-3 col-md-4 control-label label-required">Jabatan</label>
                                              <div class="col-sm-9 col-md-8 required">
                                                <input name="jabatan" class="form-control" type="text" required>
                                              </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-md-4">
                                                    <label for="s2id_autogen2" class="col-sm-3 col-md-4 control-label">Atasan</label>
                                                </div>
                                                <div class="col-md-8">
                                                  <select placeholder="Tidak Ada" class="form-control input-md" name="atasan">
                                                    <option selected value="-">Tidak Ada</option>
                                                    @foreach ($jjab as $jab)
                                                      <option value="{{$jab->jab_nama}}">{{$jab->jab_nama}}</option>
                                                    @endforeach
                                                  </select>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="OfficeUniq" class="col-sm-3 col-md-4 control-label">Kode Grup</label>
                                            <div class="col-sm-9 col-md-8">
                                              <input name="kode" class="form-control" maxlength="255" type="text">
                                            </div>
                                          </div>
                                        <div class="col-md-6">
                                            <div class="col-md-4">
                                                <label class="col-sm-3 col-md-4 control-label">Departemen</label>
                                            </div>
                                            <div class="col-md-8">
                                              <select placeholder="Tidak Ada" name="department" class="form-control input-md">
                                                  <option selected value="-">Tidak Ada</option>
                                                  @foreach ($department as $dep)
                                                    <option value="{{$dep->dept_id}}">{{$dep->dept_nama}}</option>
                                                  @endforeach
                                              </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-actions text-right">
                                    <a href="{{url('/jenisjabatan')}}" class="btn btn-success">Kembali</a>
                                    <input type="reset" value="Reset" class="btn btn-info">
                                    <button class="btn btn-danger" type="submit">
                                        Simpan
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </tbody></table>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
@endsection
