@extends('master.master')


@section('body')
  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Dashboard</li>
    </ul>
  </div>

  <form action="{{url('diklat/filterdiklat')}}" role="form" class="panel-filter" method="post">
    {{ csrf_field() }}
    <div class="panel panel-default">
        <div class="panel-heading">
            <h6 class="panel-title">Filter Data</h6>
            <div class="panel-icons-group"> <a href="#" data-panel="collapse" class="btn btn-link btn-icon"><i class="icon-arrow-up9"></i></a></div>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label>Nama</label>
                        <input name="diklat" class="form-control tip" type="text" value="" >
                    </div>
                </div>
            </div>
            <div class="form-actions text-center">
                <input type="submit" value="Reset" class="btn btn-danger btn-filter-reset">
                <input type="submit" value="Cari" class="btn btn-info btn-filter">
            </div>
        </div>
    </div>
  </form>

  <div class="panel panel-default">
    <div class="panel-body">
        <div class="block-inner text-danger">
            <h6 class="heading-hr">DATA KELUARGA
              <div class="pull-right">
                    <a data-toggle="modal" role="button" href="#form_modal" class="btn btn-xs btn-success"><i class="icon-file-plus"></i>Tambah Data</a>
              </div>
                <small class="display-block">PT Ilugroup Multimedia</small>
            </h6>
        </div>
        <!-- Form modal -->
			<div id="form_modal" class="modal fade" tabindex="-1" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title"><i class="icon-paragraph-justify2"></i> Tambah Data</h4>
						</div>

						<!-- Form inside modal -->
						<form action="{{url('/keluarga/insertkeluarga')}}" role="form" method="post">
              {{ csrf_field() }}
							<div class="modal-body with-padding">
								<div class="block-inner text-danger">
									<h6 class="heading-hr">Tambah Data Keluarga</h6>
								</div>
                <div class="form-group">
									<div class="row">
									<div class="col-sm-6">
										<label>Nama</label>
										<input type="text" placeholder="Keluarga" class="form-control" name="keluarga">
									</div>
                </div>
								</div>

							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-primary">Simpan</button>
							</div>

						</form>
					</div>
				</div>
			</div>
			<!-- /form modal -->

        <div class="table-responsive pre-scrollable stn-table">
            <form id="checkboxForm" method="post" name="checkboxForm" action="">
                <table width="100%" class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th width="50">No</th>
                            <th class="text-center">Nama</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                      @php
                        $no = 0;
                      @endphp
                      @foreach ($keluarga as $kel)
                        @php
                          $no++
                        @endphp
                        <tr id="row-1" class="removeRow1">
                            <td class="text-center">{{$no}}</td>
                            <td class="text-center">{{$kel->kel_nama}}</td>
                            <td class="text-center">
                              <form class="" action="{{ route('destroykeluarga', $kel->kel_id) }}" method="post">
                                  {{ csrf_field() }}
                                  {{ method_field('Delete') }}
                                  <button type="submit" name="submit" class="btn btn-default btn-xs btn-icon tip" data-original-title="Remove"><i class="icon-remove2"></i></button>
                                  <a data-toggle="modal" href="#{{$kel->kel_id}}"><button type="button" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Ubah"><i class="icon-pencil"></i></button></a>
                              </form>
                            </td>
                        </tr>
                        <!-- Form modal -->
                			<div id="{{$kel->kel_id}}" class="modal fade" tabindex="-1" role="dialog">
                				<div class="modal-dialog">
                					<div class="modal-content">
                						<div class="modal-header">
                							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                							<h4 class="modal-title"><i class="icon-paragraph-justify2"></i> Update Data</h4>
                						</div>

                						<!-- Form inside modal -->
                						<form action="{{route('updatekeluarga', $kel->kel_id)}}" role="form" method="post">
                              {{ csrf_field() }}
                              {{ method_field('PATCH') }}
                							<div class="modal-body with-padding">
                								<div class="block-inner text-danger">
                									<h6 class="heading-hr">Update Data Keluarga</h6>
                								</div>
                                <div class="form-group">
                									<div class="row">
                									<div class="col-sm-6">
                										<label>Nama</label>
                										<input type="text" placeholder="Keluarga" class="form-control" name="keluarga" value="{{$kel->kel_nama}}">
                									</div>
                                </div>
                								</div>

                							</div>

                							<div class="modal-footer">
                								<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                								<button type="submit" class="btn btn-primary">Update</button>
                							</div>

                						</form>
                					</div>
                				</div>
                			</div>
                			<!-- /form modal -->
                      @endforeach
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>


@endsection
