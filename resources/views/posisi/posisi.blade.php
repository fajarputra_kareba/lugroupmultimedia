@extends('master.master')

@section('body')

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
  <ul class="breadcrumb">
    <li style="color:#fff"><a href="index.html">Home</a></li>
    <li class="active">Kepegawaian</li>
  </ul>
</div>
<!-- /breadcrumbs line -->
<!-- Alert -->
<!-- <div class="alert alert-warning fade in block">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
</div> -->
<!-- /alert -->
<!-- Simple contact form -->
    <form action="{{ route('posisi.insert') }}" role="form" method="post">
          {{ csrf_field() }}
          <div class="panel panel-default">
          <div class="panel-heading"><h6 class="panel-title"><i class="icon-pencil3"></i> Form Pengisian Posisi & Jabatan</h6></div>
          <div class="panel-body">
          <div class="form-group">
            <div class="row">
              <div class="col-md-6" style="padding:10px;">
									<label>Nama:</label>
                    <select name="id" data-placeholder="Choose an option..." class="form-control" tabindex="2">
                      <option value=""></option>
                      @foreach($users as $user)
                        <option value="{{ $user->id }}" {{ $selectedUser == $user->id ? 'selected="selected"' : '' }}>{{ $user->name }}</option>
                      @endforeach
                    </select>
								</div>

                <div class="col-md-6" style="padding:10px;">
  									<label>Department:</label>
                      <select name="department" data-placeholder="Choose an option..." class="form-control" tabindex="2">
                        <option value="">Pilih Department</option>
                        @foreach($department as $dept)
                          <option value="{{ $dept->dept_nama }}" {{ $selectedUser == $dept->nama ? 'selected="selected"' : '' }}>{{ $dept->dept_nama }}</option>
                        @endforeach
                      </select>
  								</div>

                  <div class="col-md-6" style="padding:10px;">
    									<label>Jabatan:</label>
                        <select name="jabatan" data-placeholder="Choose an option..." class="form-control" tabindex="2">
                          <option value="">Pilih Jabatan</option>
                          @foreach($jabatan as $jab)
                            <option value="{{ $jab->jab_nama }}" {{ $selectedUser == $jab->nama ? 'selected="selected"' : '' }}>{{ $jab->jab_nama }}</option>
                          @endforeach
                        </select>
    							</div>

                  <div class="col-md-6" style="padding:10px;">
    									<label>Jabatan:</label>
                        <select name="tipe" data-placeholder="Choose an option..." class="form-control" tabindex="2">
                          <option value="">Tipe Pegawai</option>
                          <option value="Bulanan">Bulanan</option>
                          <option value="Harian">Harian</option>
                        </select>
    							</div>

                  <div class="col-md-6" style="padding:10px;">
                    <label>Tanggal Mulai Kerja:</label>
                        <input type="date" name="tanggal" class="form-control" required>
                  </div>

                  <div class="col-md-6" style="padding:10px;">
                    <label>Nama Cabang:</label>
                        <input type="text" name="cabang" placeholder="Nama Cabang" class="form-control" required>
                  </div>

            </div>
          </div>



          <div class="form-actions text-right">
            <input type="submit" value="Insert" class="btn btn-success">
          </div>

        </div>
      </div>
    </form>
    <!-- /simple contact form -->
@endsection
