@extends('master.master')

@section('body')

<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
  <ul class="breadcrumb">
    <li style="color:#fff"><a href="index.html">Home</a></li>
    <li class="active">Kepegawaian</li>
  </ul>
</div>
<!-- /breadcrumbs line -->
<!-- Separated form outside panel -->
@foreach ($reward as $rew)
          <div class="panel-body">
            <form class="form-horizontal form-separate" action="{{ route('pegawai.detail', $rew->rew_user_id) }}" role="form">
  						<h6><i class="icon-page-break"></i> Detail Penghargaan</h6>

                  <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Title Penghargaan:</label>
                        <div class="col-md-8">
                          <input type="text" value="{{ $rew->rew_title }}" class="form-control" disabled>
                        </div>
                    </div>
                  </div>

                <div class="col-md-6">
                    <div class="form-group">
    				            <label class="col-md-2 control-label">Pemberi Penghargaan:</label>
    				            <div class="col-md-8">
    				            	<input type="text" value="{{ $rew->rew_nama_pemberi }}" class="form-control" disabled>
    				            </div>
    				        </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
    				            <label class="col-md-2 control-label">Nomor SK:</label>
    				            <div class="col-md-8">
    				            	<input type="text" value="{{ $rew->rew_nosk }}" class="form-control" disabled>
    				            </div>
    				        </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
    				            <label class="col-md-2 control-label">Tanggal SK:</label>
    				            <div class="col-md-8">
    				            	<input type="text" value="{{ $rew->rew_tgl_keputusan }}" class="form-control" disabled>
    				            </div>
    				        </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-actions text-center">
                      <input type="submit" value="Kembali" class="btn btn-danger">
                  </div>
                  </div>

  					</form>
          </div>
@endforeach
					<!-- /separated form outside panel -->

@endsection
