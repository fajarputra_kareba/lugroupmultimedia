<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<title>iLugroup Multimedia Indonesia Management System</title>

@include('homepage.header')

</head>

<body>
	<!-- Navbar -->
  @include('homepage.nav')
	<!-- /navbar -->
	<!-- Page container -->
  @if (Route::has('login'))
      <div class="top-right links">
          @auth
            <div class="page-container">
                <!-- Sidebar -->
                @include('homepage.sidebar')
                <!-- /sidebar -->
                <!-- Page content -->
                <div class="page-content">

                    @yield('body')
                    
                    <!-- Footer -->
                    @include('homepage.footer')
                    <!-- /footer -->
                </div>
                <!-- /page content -->
              </div>
              <!-- /page container -->
          @else
              @include('login')
          @endauth
      </div>
  @endif

</body>


</html>
