@extends('master.master')


@section('body')
  <!-- Breadcrumbs line -->
  <div class="breadcrumb-line">
    <ul class="breadcrumb">
      <li style="color:#fff"><a href="index.html">Home</a></li>
      <li class="active">Dashboard</li>
    </ul>
  </div>

  <form action="{{url('diklat/filterdiklat')}}" role="form" class="panel-filter" method="post">
    {{ csrf_field() }}
    <div class="panel panel-default">
        <div class="panel-heading">
            <h6 class="panel-title">Filter Data</h6>
            <div class="panel-icons-group"> <a href="#" data-panel="collapse" class="btn btn-link btn-icon"><i class="icon-arrow-up9"></i></a></div>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label>Nama Tingkat Pendidikan</label>
                        <input name="diklat" class="form-control tip" type="text" value="" >
                    </div>
                </div>
            </div>
            <div class="form-actions text-center">
                <input type="submit" value="Reset" class="btn btn-danger btn-filter-reset">
                <input type="submit" value="Cari" class="btn btn-info btn-filter">
            </div>
        </div>
    </div>
  </form>

  <div class="panel panel-default">
    <div class="panel-body">
        <div class="block-inner text-danger">
            <h6 class="heading-hr">DATA Tingkat Pendidikan
              <div class="pull-right">
                    <a href="{{url('jpendidikan/createjpendidikan')}}"><button class="btn btn-xs btn-success" type="button"><i class="icon-file-plus"></i>Tambah Data</button></a>
              </div>
                <small class="display-block">PT Ilugroup Multimedia</small>
            </h6>
        </div>
        <div class="table-responsive pre-scrollable stn-table">
            <form id="checkboxForm" method="post" name="checkboxForm" action="">
                <table width="100%" class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th width="50">No</th>
                            <th class="text-center">Tingkat Pendidikan</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                      @php
                        $no = 0;
                      @endphp
                      @foreach ($jpen as $key => $j)
                        @php
                          $no++
                        @endphp
                        <tr id="row-1" class="removeRow1">
                            <td class="text-center">{{$no}}</td>
                            <td class="text-center">{{$j->jpen_tingkat}}</td>
                            <td class="text-center">
                              <form class="" action="{{ route('destroyjpendidikan', $j->jpen_id) }}" method="post">
                                  {{ csrf_field() }}
                                  {{ method_field('Delete') }}
                                  <button type="submit" name="submit" class="btn btn-default btn-xs btn-icon tip" data-original-title="Remove"><i class="icon-remove2"></i></button>
                                  <a href="{{url('jpendidikan/'.$j->jpen_id.'/editjpendidikan')}}"><button type="button" class="btn btn-default btn-xs btn-icon tip" title="" data-original-title="Ubah"><i class="icon-pencil"></i></button></a>
                              </form>
                            </td>
                        </tr>
                      @endforeach
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>


@endsection
